var apiContext = require('mozu-node-sdk/clients/platform/application')();

var getDocuments = function(){
    var requestBody = {
        documentListName    : 'files@mozu',
        documentTypeFQN     : 'image@mozu',
        'filter'            : 'id eq "1653afcf-b07f-466b-bfae-3a5916ae45ad"'
    };
    // return documentResource.getDocuments(requestBody).then(function(response){
    //     console.log(response);
    // }).catch(function(error){
    //     console.log("Custom Schema Document Service :  getDocuments : "+error.message);
    // });
    apiContext.context.tenant = 17149;
    var documentResource     = require('mozu-node-sdk/clients/content/documentlists/document')(apiContext);
    var documentTResource     = require('mozu-node-sdk/clients/content/documentList')(apiContext);
    return documentResource.getDocuments(requestBody).then(function(response){
        console.log(response);
    }).catch(function(error){
        console.log("Custom Schema Document Service :  getDocuments : "+error.message);
    });
}

getDocuments();
