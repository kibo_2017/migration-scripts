var apiContext              = require('mozu-node-sdk/clients/platform/application')();
// apiContext.context.tenant = 22834;
apiContext.context.tenant = 17149;
var documentResource     = require('mozu-node-sdk/clients/content/documentlists/document')(apiContext);

var createDocument = function(documentObject, rownumber){
    documentResource.createDocument(documentObject).then(function(response){
        console.log("Created :  🍺   🍺   🍺 "+rownumber);
    }).catch(function(error){
        console.log(error.message+" : "+rownumber);
    });
}


var KIBO_SERVICE = {
    'createDocument' : createDocument
}
module.exports = KIBO_SERVICE;


// LabellersTypewriters
// Fax-Machines --> FaxMachines
// Labellers
// Mobile-Printers  ---> MobilePrinters
// Printers-Multifunctions
// Scanners
// Supplies-Accessories
// Typewriters

// CuttingMachines
// Sergers
// Sewing,Quilting-EmbroideryMachines
// Software
