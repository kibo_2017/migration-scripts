var apiContext              = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = 21830;
apiContext.context['master-catalog'] = 2;
var documentResource     = require('mozu-node-sdk/clients/content/documentlists/document')(apiContext);

var FILE_NAME = './product_specification/product_specification_data.csv';
var DOCUMENT_TYPE_FQN = "grouping_specification_type_1@Brother";
var LIST_FQN = "grouping_specification_list_1@Brother";
var DOCUMENT_TYPE = "grouping_specification_type_1",
    DOCUMENT_LIST = "grouping_specification_list_1",
    DOCUMENT_VIEW = "grouping_specification_view_1",
    NAMESPACE = "Brother";

var start = function(){
    // Get all the documents from teant
    documentResource.getDocuments({
        documentListName:DOCUMENT_LIST+'@'+NAMESPACE,
        startIndex: 0,
        pageSize: 200,
        filter: 'properties.group_name_en eq "Hardware Featrues"'
    }).then(function(response){
        console.log(response.items.length);
        console.log("\n\n");
        // console.log(JSON.stringify(response));
        updateinfo(response.items);
    }).catch(function(error){
        console.log(error);
    });
}

var updateinfo =  function(items){
    // for (var i = 0; i < 2; i++) {
    for (var i = 0; i < items.length; i++) {
        items[i].properties.group_name_en = 'Hardware Features';
        items[i].documentListName = items[i].listFQN;
        items[i].documentId = items[i].id;
        documentResource.updateDocument(items[i]).then(function(upd){
            console.log("\t"+upd);
        }).catch(function(err){
            console.log(err);
        });
    }
}

start();
