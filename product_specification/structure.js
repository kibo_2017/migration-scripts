
var SITE_INFOR_DOCUMENT_TYPE = "grouping_specification_type_1",
    SITE_INFOR_DOCUMENT_LIST = "grouping_specification_list_1",
    SITE_INFOR_DOCUMENT_VIEW = "grouping_specification_view_1",
    NAMESPACE = "Brother",
    // NAMESPACE = "echidna",
    TENANT_ID = 22834,
    SITE_IDS = [];


var SITE_INFOR_DOCUMENT_TYPE_STRUCTURE = {
        documentTypeFQN : SITE_INFOR_DOCUMENT_TYPE+'@'+NAMESPACE,
        name            : SITE_INFOR_DOCUMENT_TYPE,
        namespace       : NAMESPACE,
        metadata        : {
            isWebPage   : true
        },
        properties      : [
            {
                name            : 'attribute_code',
                isRequired      : true,
                isMultiValued   : false,
                propertyType    :
                {
                    name                : 'string',
                    namespace           : 'mozu',
                    propertyTypeFQN     : 'string@mozu',
                    installationPackage : 'core@mozu',
                    version             : '1.0',
                    dataType            : 'string'
                }
            },
            {
                name            : 'group_name_en',
                isRequired      : true,
                isMultiValued   : false,
                propertyType    :
                {
                    name                : 'string',
                    namespace           : 'mozu',
                    propertyTypeFQN     : 'string@mozu',
                    installationPackage : 'core@mozu',
                    version             : '1.0',
                    dataType            : 'string'
                }
            },
            {
                name            : 'group_name_fr',
                isRequired      : true,
                isMultiValued   : false,
                propertyType    :
                {
                    name                : 'string',
                    namespace           : 'mozu',
                    propertyTypeFQN     : 'string@mozu',
                    installationPackage : 'core@mozu',
                    version             : '1.0',
                    dataType            : 'string'
                }
            },
            {
                name            : 'product_type',
                isRequired      : true,
                isMultiValued   : false,
                propertyType    :
                {
                    name                : 'string',
                    namespace           : 'mozu',
                    propertyTypeFQN     : 'string@mozu',
                    installationPackage : 'core@mozu',
                    version             : '1.0',
                    dataType            : 'string'
                }
            },
            {
                name            : 'sorting_order',
                isRequired      : true,
                isMultiValued   : false,
                propertyType    :
                {
                    name                : 'int',
                    namespace           : 'mozu',
                    propertyTypeFQN     : 'string@mozu',
                    installationPackage : 'core@mozu',
                    version             : '1.0',
                    dataType            : 'string'
                }
            }
        ]
};


var SITE_INFOR_DOCUMENT_LIST_STRUCTURE = {
    name                        : SITE_INFOR_DOCUMENT_LIST,
    namespace                   : NAMESPACE,
    documentTypes               : [SITE_INFOR_DOCUMENT_TYPE+'@'+NAMESPACE],
    listFQN                     : SITE_INFOR_DOCUMENT_LIST+'@'+NAMESPACE,
    views   : [
        {
            name                    : SITE_INFOR_DOCUMENT_VIEW,
            isVisibleInStorefront   : true,
            fields  : [
                {
                    name            : 'attribute_code',
                    target          : 'properties.attribute_code'
                },
                {
                    name            : 'group_name_en',
                    target          : 'properties.group_name_en'
                },
                {
                    name            : 'group_name_fr',
                    target          : 'properties.group_name_fr'
                },
                {
                    name            : 'product_type',
                    target          : 'properties.product_type'
                },
                {
                    name            : 'sorting_order',
                    target          : 'properties.sorting_order'
                }
            ]
        }
    ],
    usages  : [
        "siteBuilder",
        "entityManager"
    ],
    scopeType   : 'Tenant'
};




var createDocumentType = function(){
    var apiContext              = require('mozu-node-sdk/clients/platform/application')();
    apiContext.context.tenant = 21830;
    var documentListResource    = require('mozu-node-sdk/clients/content/documentList')(apiContext);
    var documentTypeResource    = require('mozu-node-sdk/clients/content/documentType')(apiContext);


    console.log(SITE_INFOR_DOCUMENT_TYPE_STRUCTURE);
    documentTypeResource.createDocumentType(SITE_INFOR_DOCUMENT_TYPE_STRUCTURE).then(function(resp){
        console.log(resp);
        console.log("Created SITE_INFOR_DOCUMENT_TYPE_STRUCTURE");
        // createDocumentList();
    }).catch(function(error) {
        console.log(error);
        // throw new Error("SITE_INFOR_DOCUMENT_TYPE_STRUCTURE"+error.message);
    });
}

//
var createDocumentList = function(data){
    var apiContext              = require('mozu-node-sdk/clients/platform/application')();
    apiContext.context.tenant = 21830;
    var documentListResource    = require('mozu-node-sdk/clients/content/documentList')(apiContext);
    var documentTypeResource    = require('mozu-node-sdk/clients/content/documentType')(apiContext);

    documentListResource.createDocumentList(SITE_INFOR_DOCUMENT_LIST_STRUCTURE).then(function(resp){
        console.log("Created SITE_INFOR_DOCUMENT_LIST_STRUCTURE");
    }).catch(function(error) {
        throw new Error(error.message);
    });
}
