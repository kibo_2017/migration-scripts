/**
 * This script do following activities:
 * Create required document type and list for storing the product specification informations.
 * Read data from the CSV file and upload them into KIBO tenant.
 * Migration status be stored into a product_specification_migration_<DATE>.csv file
 * Smaple migration file is kept in current folder, follow the same structure to uploda data.
**/

/**
 *  GLOBAL CONSTANTS
**/
var FILE_NAME = './product_specification/product_specification_data.csv';
var DOCUMENT_TYPE_FQN = "grouping_specification_type_1@Brother";
var LIST_FQN = "grouping_specification_list_1@Brother";
var DOCUMENT_TYPE = "grouping_specification_type_1",
    DOCUMENT_LIST = "grouping_specification_list_1",
    DOCUMENT_VIEW = "grouping_specification_view_1",
    NAMESPACE = "Brother";

// 25 old was there in UAT already
// 849+199
var TENANT = 22834;
var PRODUCTION_TENANT = 21830;
    var apiContext              = require('mozu-node-sdk/clients/platform/application')();
    // apiContext.context.tenant = 22834;
    apiContext.context.tenant = TENANT;
    var documentResource     = require('mozu-node-sdk/clients/content/documentlists/document')(apiContext);
/**
 * Modular imports
**/
var fs = require('fs');
var csv = require("fast-csv");
var uuid = require('uuid');
var KIBO_SERVICE = require('./kibo_service');

function writeDataIntoCSVfile(){
    var csvStream = csv
        .format({headers: true})
        .transform(function(row, next){
            setImmediate(function(){
                next(null, {
                    attribute_code: row.attribute_code,
                    group_name_en: row.group_name_en,
                    group_name_fr: row.group_name_fr,
                    product_type: row.product_type,
                    sorting_order: row.sorting_order,
                });
            });;
        }),
        writableStream = fs.createWriteStream(FILE_NAME);

    writableStream.on("finish", function(){
      console.log("DONE!");
    });
    var newData = {"success":true,"total":9,"items":[{"id":"e5d42819-a6e8-4451-a9b6-54b18ed00b10","name":"e23c4190-0292-11e7-ab58-6db380bf465f","extension":"","documentTypeFQN":"grouping_specification_type_1@Brother","listFQN":"grouping_specification_list_1@Brother","publishState":"none","properties":{"attribute_code":"tenant~s-general-lcd-display","group_name_en":"Features","group_name_fr":"Fonctionnalites","product_type":"scanners","sorting_order":"20"},"insertDate":"2017-03-06T17:32:34.926Z","updateDate":"2017-03-07T11:24:20.525Z","startDate":null,"endDate":null,"listFlags":{"supportsADR":false,"enableADR":false,"supportsPublishing":false,"enablePublishing":false},"entityType":"cms"},{"id":"753b981d-ec0f-45fa-958c-a32e04c6de77","name":"e23c4192-0292-11e7-ab58-6db380bf465f","extension":"","documentTypeFQN":"grouping_specification_type_1@Brother","listFQN":"grouping_specification_list_1@Brother","publishState":"none","properties":{"attribute_code":"tenant~s-paper-handling-media-type-s-","group_name_en":"Other","group_name_fr":"Autre","product_type":"scanners","sorting_order":"30"},"insertDate":"2017-03-06T17:32:34.941Z","updateDate":"2017-03-07T11:24:20.540Z","startDate":null,"endDate":null,"listFlags":{"supportsADR":false,"enableADR":false,"supportsPublishing":false,"enablePublishing":false},"entityType":"cms"},{"id":"0f2acb34-010a-4923-9579-f7b80095c16c","name":"e23c4193-0292-11e7-ab58-6db380bf465f","extension":"","documentTypeFQN":"grouping_specification_type_1@Brother","listFQN":"grouping_specification_list_1@Brother","publishState":"none","properties":{"attribute_code":"tenant~s-paper-handling-multipurposet","group_name_en":"Other","group_name_fr":"Autre","product_type":"scanners","sorting_order":"30"},"insertDate":"2017-03-06T17:32:35.004Z","updateDate":"2017-03-07T11:24:20.667Z","startDate":null,"endDate":null,"listFlags":{"supportsADR":false,"enableADR":false,"supportsPublishing":false,"enablePublishing":false},"entityType":"cms"},{"id":"07431b62-73c2-4ba7-b634-0ad00ec0775d","name":"e23c4191-0292-11e7-ab58-6db380bf465f","extension":"","documentTypeFQN":"grouping_specification_type_1@Brother","listFQN":"grouping_specification_list_1@Brother","publishState":"none","properties":{"attribute_code":"tenant~s-general-toner-save-mode","group_name_en":"Other","group_name_fr":"Autre","product_type":"scanners","sorting_order":"30"},"insertDate":"2017-03-06T17:32:34.941Z","updateDate":"2017-03-07T11:24:20.525Z","startDate":null,"endDate":null,"listFlags":{"supportsADR":false,"enableADR":false,"supportsPublishing":false,"enablePublishing":false},"entityType":"cms"},{"id":"6d2f12b6-09fd-4c9d-87dc-3d4cb1899f21","name":"cutting-machines","publishSetCode":"","extension":"","documentTypeFQN":"grouping_specification_type_1@Brother","listFQN":"grouping_specification_list_1@Brother","publishState":"none","properties":{"attribute_code":"tenant~s-general-print-technology","group_name_en":"Print","group_name_fr":"Impression","product_type":"Cutting Machines","sorting_order":"10"},"insertDate":"2017-03-20T10:57:18.353Z","updateDate":"2017-03-20T10:57:18.353Z","startDate":null,"endDate":null,"listFlags":{"supportsADR":false,"enableADR":false,"supportsPublishing":false,"enablePublishing":false},"entityType":"cms"},{"id":"88cab0ce-e813-49fa-abaf-99488fba4010","name":"e23bcc60-0292-11e7-ab58-6db380bf465f","extension":"","documentTypeFQN":"grouping_specification_type_1@Brother","listFQN":"grouping_specification_list_1@Brother","publishState":"none","properties":{"attribute_code":"tenant~s-general-print-technology","group_name_en":"Print","group_name_fr":"Impression","product_type":"scanners","sorting_order":"10"},"insertDate":"2017-03-06T17:32:34.791Z","updateDate":"2017-03-07T11:24:20.525Z","startDate":null,"endDate":null,"listFlags":{"supportsADR":false,"enableADR":false,"supportsPublishing":false,"enablePublishing":false},"entityType":"cms"},{"id":"c6c02bcf-04db-4713-89ff-0e4f952a9aca","name":"some-name-unique","publishSetCode":"","extension":"","documentTypeFQN":"grouping_specification_type_1@Brother","listFQN":"grouping_specification_list_1@Brother","publishState":"none","properties":{"attribute_code":"Attribute code","group_name_en":"en name","group_name_fr":"french name","product_type":"product type","sorting_order":"order"},"insertDate":"2017-03-18T08:32:04.275Z","updateDate":"2017-03-18T08:32:04.275Z","startDate":null,"endDate":null,"listFlags":{"supportsADR":false,"enableADR":false,"supportsPublishing":false,"enablePublishing":false},"entityType":"cms"},{"id":"7a4eb7e8-2001-41d6-8eb5-f7321e210ef1","name":"printers-multifunctions","publishSetCode":"","extension":"","documentTypeFQN":"grouping_specification_type_1@Brother","listFQN":"grouping_specification_list_1@Brother","publishState":"none","properties":{"attribute_code":"tenant~s-general-print-technology","group_name_en":"Print","group_name_fr":"Impression","product_type":"Printers & Multifunctions","sorting_order":"10"},"insertDate":"2017-03-28T11:28:37.973Z","updateDate":"2017-03-28T11:28:37.973Z","startDate":null,"endDate":null,"listFlags":{"supportsADR":false,"enableADR":false,"supportsPublishing":false,"enablePublishing":false},"entityType":"cms"},{"id":"7ea1ebea-d022-4053-8ab2-cbe9fcb541e2","name":"e23c4194-0292-11e7-ab58-6db380bf465f","extension":"","documentTypeFQN":"grouping_specification_type_1@Brother","listFQN":"grouping_specification_list_1@Brother","publishState":"none","properties":{"attribute_code":"tenant~s-paper-handling-optional-pape","group_name_en":"Other","group_name_fr":"Autre","product_type":"scanners","sorting_order":"30"},"insertDate":"2017-03-06T17:32:34.807Z","updateDate":"2017-03-07T11:24:21.400Z","startDate":null,"endDate":null,"listFlags":{"supportsADR":false,"enableADR":false,"supportsPublishing":false,"enablePublishing":false},"entityType":"cms"}]};

    csvStream.pipe(writableStream);
    for (var i = 0; i < newData.items.length; i++) {
        csvStream.write({
            attribute_code  : newData.items[i].properties.attribute_code,
            group_name_en   : newData.items[i].properties.group_name_en,
            group_name_fr   : newData.items[i].properties.group_name_fr,
            product_type    : newData.items[i].properties.product_type,
            sorting_order   : newData.items[i].properties.sorting_order
        });
    }
    csvStream.end();
}


/**
 * Functionality to read product sepecification data from a CSV file and migrate
 * it to KIBO custom schema document list.
**/
function readDataFromCSVfile(){
    var stream = fs.createReadStream(FILE_NAME);
    var dataObjects = [];
    try {
        var csvStream = csv().on("data", function(data){
            dataObjects.push(data);
        }).on("end", function(){
            console.log("\n\tCompleated Reading Data from file : Number of rows in "+FILE_NAME+' '+ dataObjects.length);
            insterProductSpecification(dataObjects);
        });
      stream.pipe(csvStream);
    } catch (e) {
      console.log(e.message);
    }
}

var XLSX = require('xlsx');
var xlsxData;
function readDataFromXSLSXfile(start, max){
    var workbook = XLSX.readFile('./product_specification/HAD-Attribute-Groupings.xlsx');
    console.log(workbook.SheetNames);
    var worksheet = workbook.Sheets[workbook.SheetNames[0]];

    xlsxData = XLSX.utils.sheet_to_json(worksheet, {header:1});
    console.log(xlsxData.length);
    readDataAndUpdate(start,max);
}

var readDataAndUpdate = function(start, max){
    for (var i = start; i < xlsxData.length && i < max; i++) {
        // console.log(xlsxData[i]);
        insterProductSpecification2(xlsxData[i], start, max, i);
    }
}

function insterProductSpecification2(dataObjects, start, max, i){
    if(dataObjects[0] && dataObjects[1] && dataObjects[2] &&
        dataObjects[3] && dataObjects[4]){
            var documentObject = {};
            documentObject['properties'] = {};
            documentObject['properties']['attribute_code'] = 'tenant~'+dataObjects[0];
            documentObject['properties']['group_name_en'] = dataObjects[1];
            documentObject['properties']['group_name_fr'] = dataObjects[2];
            documentObject['properties']['product_type'] = dataObjects[3];
            documentObject['properties']['sorting_order'] = dataObjects[4];

            documentObject['name'] = uuid.v1();
            documentObject['documentListName'] = DOCUMENT_LIST+'@'+NAMESPACE;
            documentObject['documentTypeFQN'] = DOCUMENT_TYPE+'@'+NAMESPACE;

            createDocument(documentObject, i, start, max);
    }else{
        console.log("Validation error for : "+ i);
    }
}

var COUNT = 0;
var createDocument = function(documentObject, rownumber, start, max){
    var apiContext              = require('mozu-node-sdk/clients/platform/application')();
    // apiContext.context.tenant = 22834;
    apiContext.context.tenant = TENANT;
    var documentResource     = require('mozu-node-sdk/clients/content/documentlists/document')(apiContext);
    documentResource.createDocument(documentObject).then(function(response){
        process.stdout.write(" CREATED "+rownumber);
        if(rownumber == max-1){
            readDataAndUpdate(max, max+50);
        }
    }).catch(function(error){
        process.stdout.write("\n\t\t"+start+" == "+max+"Error "+rownumber+"\n");
        if(rownumber == max-1){
            readDataAndUpdate(max, max+50);
        }
    });
}

function insterProductSpecification(dataObjects){
    for (var i = 0; i<dataObjects.length ; i++) {
        if(dataObjects[i][0] && dataObjects[i][1] && dataObjects[i][2] &&
            dataObjects[i][3] && dataObjects[i][4]){
                var documentObject = {};
                documentObject['properties'] = {};
                documentObject['properties']['attribute_code'] = dataObjects[i][0];
                documentObject['properties']['group_name_en'] = dataObjects[i][1];
                documentObject['properties']['group_name_fr'] = dataObjects[i][2];
                documentObject['properties']['product_type'] = dataObjects[i][3];
                documentObject['properties']['sorting_order'] = dataObjects[i][4]+'';

                documentObject['name'] = uuid.v1();
                documentObject['documentListName'] = DOCUMENT_LIST+'@'+NAMESPACE;
                documentObject['documentTypeFQN'] = DOCUMENT_TYPE+'@'+NAMESPACE;

                KIBO_SERVICE.createDocument(documentObject,i);
        }else{
            console.log("Validation error for : "+ i);
        }
    }
}
var deleteAll =  function(){
    documentResource.getDocuments({
        documentListName : DOCUMENT_LIST+'@'+NAMESPACE,
        pageSize: 50,
        filter: 'properties.product_type eq "Software"'
    }).then(function(response){
        // console.log(response);
        deleteDoc(response);
    }).catch(function(err){
        console.log(err);
    });
}

var deleteDoc =  function(response){
    var count = 0 ;
    for (var i = 0; i < response.items.length; i++) {
        // response.items[i]

        documentResource.deleteDocument({
            documentListName : DOCUMENT_LIST+'@'+NAMESPACE,
            documentId : response.items[i].id
        }).then(function(){
            process.stdout.write("*");
            count = count + 1;
            if(response.items.length == count){
                console.log("Calling Agian");
                deleteAll();
            }
        }).catch(function(){
            process.stdout.write("NA");
            count = count + 1;
            if(response.items.length == count){
                console.log("Calling Agian");
                deleteAll();
            }
        });
    }
}

var getDocumentAndUpdate = function(){
    var apiContext              = require('mozu-node-sdk/clients/platform/application')();
    // apiContext.context.tenant = 22834;
    apiContext.context.tenant = TENANT;
    var documentResource     = require('mozu-node-sdk/clients/content/documentlists/document')(apiContext);
    documentResource.getDocuments({
        documentListName:DOCUMENT_LIST+'@'+NAMESPACE,
        filter:'properties.group_name_en eq "Hardware Featrues"',
        startIndex: 0,
        pageSize: 200
    }).then(function(response){
        console.log(JSON.stringify(response.totalCount));
        for (var i = 0; i < response.items.length; i++) {

            if(response.items[i].properties.group_name_en == 'Hardware Featrues'){
                response.items[i].properties.group_name_en = 'Hardware Features';
                response.items[i].documentListName = response.items[i].listFQN;
                response.items[i].documentId = response.items[i].id;
                documentResource.updateDocument(response.items[i]).then(function(upd){
                    console.log("\t"+upd);
                }).catch(function(err){
                    console.log(err);
                });
            }
        }
        // for (var i = 0; i < response.items.length; i++) {
        //
        //     response.items[i].properties.product_type = 'MobilePrinters';
        //     console.log(response.items[i].properties.product_type);
        //     console.log(response.items[i]);
        //     response.items[i].documentListName = response.items[i].listFQN;
        //     response.items[i].documentId = response.items[i].id;
        //     documentResource.updateDocument(response.items[i]).then(function(upd){
        //         console.log("\t\t\t\t"+upd);
        //     }).catch(function(err){
        //         console.log(err);
        //     });
        // }
    }).catch(function(error){
        console.log(error);
    });
}
getDocumentAndUpdate();


// readDataFromCSVfile();
// readDataFromXSLSXfile(1, 50);
// deleteAll();

var pushToUat =  function(allOrgData){
    for (var i = 0; i < allOrgData.length; i++) {
        var documentObject = {};
        documentObject['properties'] = {};
        documentObject['properties']['attribute_code'] = allOrgData[i]['properties']['attribute_code'];
        documentObject['properties']['group_name_en'] = allOrgData[i]['properties']['group_name_en'];
        documentObject['properties']['group_name_fr'] = allOrgData[i]['properties']['group_name_fr'];
        documentObject['properties']['product_type'] = allOrgData[i]['properties']['product_type'];
        documentObject['properties']['sorting_order'] = allOrgData[i]['properties']['sorting_order'];

        documentObject['name'] = allOrgData[i].id;
        documentObject['documentListName'] = DOCUMENT_LIST+'@'+NAMESPACE;
        documentObject['documentTypeFQN'] = DOCUMENT_TYPE+'@'+NAMESPACE;
        cre(documentObject);

    }
}

var cre =  function(documentObject){
    var apiContext              = require('mozu-node-sdk/clients/platform/application')();
    // apiContext.context.tenant = 22834;
    apiContext.context.tenant = PRODUCTION_TENANT;
    var documentResource     = require('mozu-node-sdk/clients/content/documentlists/document')(apiContext);
    documentResource.createDocument(documentObject).then(function(response){
        console.log("CREATED");
    }).catch(function(error){
        console.log(error);
        console.log(documentObject);
    });
}

var allData = [];
var allOrgData = [];
var exportData =  function(startIndex){
    var apiContext              = require('mozu-node-sdk/clients/platform/application')();
    // apiContext.context.tenant = 22834;
    // apiContext.context.tenant = TENANT;
    apiContext.context.tenant = PRODUCTION_TENANT;
    var documentResource     = require('mozu-node-sdk/clients/content/documentlists/document')(apiContext);
    documentResource.getDocuments({
        documentListName:DOCUMENT_LIST+'@'+NAMESPACE,
        startIndex: startIndex,
        pageSize: 200
    }).then(function(response){
        allOrgData = allOrgData.concat(response.items);
        for (var i = 0; i < response.items.length; i++) {
            var temp = response.items[i].properties;
            temp.id = response.items[i].id;
            allData.push(temp);
        }
        if(allData.length < response.totalCount){
            exportData(response.startIndex+200);
        }else {
            console.log(allData.length);
            console.log(allOrgData[0]);
            allData.sort(function(a,b){
                return a.id < b.id;
            });
            var ws = fs.createWriteStream("PRODUCTION_PRODUCT_SPECIFICATION.csv");
            // var ws = fs.createWriteStream("UAT_PRODUCT_SPECIFICATION.csv");
            csv.write(allData, {headers: true}).pipe(ws);

            // pushToUat(allOrgData,0);
        }
    }).catch(function(error){
        console.log(error);
    });
}

// exportData(0);
