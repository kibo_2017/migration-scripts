var DEV_TENANT_ID = 17149;
var QA_TENANT_ID  = 17485;
var DEMO_TENANT_ID = 17406;
var UAT_TENANT_ID = 22834;

var fs = require('fs');
var XLSX = require('xlsx');

var slugify = require('slugify')
var apiContext      = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = UAT_TENANT_ID;
apiContext.context['master-catalog'] = 1;
var productResource = require('mozu-node-sdk/clients/commerce/catalog/admin/product')(apiContext);

var getProduct = function(pcode, infos){
    productResource.getProduct({productCode: pcode}).then(function(resp){
        console.log(JSON.stringify(resp));
        updateProductByBatch(resp, infos);
    }).catch(function(err){
        console.log(err);
        console.log(pcode);
    });
}

var updateProductByBatch = function(productToUpdate, infos){
    console.log(infos); 
    for (var i = 0; i < productToUpdate.productInCatalogs.length; i++) {
        if(infos[0].indexOf('English')>-1){
            if(productToUpdate.productInCatalogs.localeCode == 'en-CA'){
                productToUpdate.productInCatalogs[i].seoContent.metaTagTitle = infos[2]?infos[2]: productToUpdate.content.productName;
                productToUpdate.productInCatalogs[i].seoContent.metaTagDescription = infos[3];
                productToUpdate.productInCatalogs[i].seoContent.metaTagKeywords = infos[4];
                productToUpdate.productInCatalogs[i].seoContent.seoFriendlyUrl = slugify(infos[5]?infos[5]: productToUpdate.content.productName);

                productToUpdate.seoContent.metaTagTitle = infos[2]?infos[2]: productToUpdate.content.productName;
                productToUpdate.seoContent.metaTagDescription = infos[3];
                productToUpdate.seoContent.metaTagKeywords = infos[4];
                productToUpdate.seoContent.seoFriendlyUrl = slugify(infos[5]?infos[5]: productToUpdate.content.productName);

                updateP(productToUpdate);
            }
        }else{
            if(productToUpdate.productInCatalogs.localeCode == 'fr-CA'){
                productToUpdate.productInCatalogs[i].seoContent.metaTagTitle = infos[2]?infos[2]: productToUpdate.content.productName;
                productToUpdate.productInCatalogs[i].seoContent.metaTagDescription = infos[3];
                productToUpdate.productInCatalogs[i].seoContent.metaTagKeywords = infos[4];
                productToUpdate.productInCatalogs[i].seoContent.seoFriendlyUrl = slugify(infos[5]?infos[5]: productToUpdate.content.productName);
            }
        }
    }
}

var updateP = function(productToUpdate){
    console.log(productToUpdate.productCode);
    productResource.updateProduct(productToUpdate).then(function(resp){
        console.log(productToUpdate.productCode);
    }).catch(function(err){
        console.log(err);
        console.log('ERRER!');
    });
}


var readData =  function(){
    var workbook = XLSX.readFile('./products/Top-Products-Meta-description--Keywords-EN-FR.xlsx');
    console.log(workbook.SheetNames);
    var worksheet = workbook.Sheets[workbook.SheetNames[0]];

    xlsxData = XLSX.utils.sheet_to_json(worksheet, {header:1});
    console.log(xlsxData.length);
    console.log(xlsxData[1]);
    console.log(xlsxData[1].length);
    // for (var i = 1; i < xlsxData.length; i++) {
    for (var i = 1; i < 2; i++) {
        getProduct(xlsxData[i][1],xlsxData[i]);
    }
}

readData();
