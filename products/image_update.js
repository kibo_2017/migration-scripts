var apiContext      = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = 22834;
// apiContext.context.siteId = 27154;
var productResource = require('mozu-node-sdk/clients/commerce/catalog/admin/product')(apiContext);
var XLSX = require('xlsx');
var csvFile = './products/DevProductImagesID.xlsx';

var start =  function(){
    // first we will tell our program to read the file.
    // readProductImageMappingInformationFromFile();
}

/**
 * convert key to values array into a js object with,
 *  keys are property and values are vallue for each property
 **/
 var productImageMap = [];
 var COUNTER =  0;

 var consoleLog = function(str,error){
     process.stdout.write(str+'');
 }

var pushImage =  function(value){
    var object = {}, isExist =  false;
    object['productCode'] = value[1];
    object['cmsId'] = value[3];
    object['sequence'] = value[4];
    for (var i = 0; i < productImageMap.length; i++) {

        if(productImageMap[i].productCode == object.productCode){
            isExist = true;
            productImageMap[i].cmsIds.push({
                id: object.cmsId,
                sequence: object.sequence
            });
        }
    }
    if(!isExist){
        productImageMap.push({
            productCode: object.productCode,
            cmsIds : [{
                id: object.cmsId,
                sequence: object.sequence
            }]
        });
    }
}

// Reorder cmsId by sequence
var reOrderBySequence = function(){
    for (var i = 0; i < productImageMap.length; i++) {
        // productImageMap[i].cmsIds
        productImageMap[i].cmsIds.sort(function(a, b) {
          return a.sequence - b.sequence;
        });
    }
}

// Functionality to read data from the file.
var readProductImageMappingInformationFromFile =  function(){
    try {
        // let open file, make sure the path is correct
        var workbook = XLSX.readFile(csvFile);
        // As we know our data is reside in firt sheet, lets open that
        var worksheet = workbook.Sheets[workbook.SheetNames[0]];
        // Convert data into a matrix strucutre ;), the best way to process
        // multi dimentional data! My Grandpa told me this.
        var xlsxData = XLSX.utils.sheet_to_json(worksheet, {header:1});
        // first row will be the properties as we know its a xlsx data :P
        var headers = xlsxData[0];
        console.log("Okey! these are the values in the sheet, not bad.");
        console.log(headers);
        console.log("\n");

        for (var i = 1; i < xlsxData.length; i++) {
            // Convert each row item into and object with proper mapping
            for (var i = 0; i < xlsxData.length; i++) {
                pushImage(xlsxData[i]);
            }
        }

        reOrderBySequence();
        // console.log(JSON.stringify(productImageMap));
        updateProductImage(productImageMap);
    } catch (e) {
        console.log(e);
    }
}

var updateProductImage =  function(productImageMap){
    for (var i = 0; i < productImageMap.length; i++) {
    // for (var i = 1; i < 2; i++) {
        getProduct(productImageMap[i]);
    }
}

var getProduct = function(productImageMapinfo){
    var apiContext      = require('mozu-node-sdk/clients/platform/application')();
    apiContext.context.tenant = 22834;
    // apiContext.context.siteId = 33271;
    var productResource = require('mozu-node-sdk/clients/commerce/catalog/admin/product')(apiContext);
    // console.log(productImageMapinfo);
    // productResource.getProduct({productCode:productImageMapinfo.productCode}).then(function(response){
    productResource.getProduct({productCode:productImageMapinfo.productCode}).then(function(response){
        consoleLog("|");
        updateProduct(response,productImageMapinfo)
    }).catch(function(error){
        console.log("No product found!  : ");
        console.log(productImageMapinfo);
    });
}

var updateProduct =  function(product, productImageMapinfo){
    // console.log(JSON.stringify(product));
    product.content['productImages'] = [];
    // console.log(productImageMapinfo.cmsIds);
    for (var i = 0; i < productImageMapinfo.cmsIds.length; i++) {
        product.content['productImages'].push({'cmsId':productImageMapinfo.cmsIds[i].id});
    }

    productResource.updateProduct(product).then(function(response){
        // console.log(JSON.stringify(response));
        COUNTER = COUNTER+1;
        consoleLog("#"+COUNTER);
    }).catch(function(error){
        console.log("Update Failed for : ");
        console.log(productImageMapinfo);
    });
}



var start2 =  function(){
    var codes = [
        "1134D",
        "4234DT",
        "BM2800",
        "CM350",
        "CM650W",
        "CP6500",
        "CP7500",
        "DCPL2520DW",
        "DCPL2540DW",
        "HE240",
        "HL3140CW",
        "HL3170CDW",
        "HL3180CDW",
        "HLL2320D",
        "HLL2360DW",
        "HLL2380DW",
        "HLL5000D",
        "HLL5200DW",
        "HLL6200DW",
        "HLL6400DW",
        "HLL8350CDW",
        "HLL9200CDW",
        "HL1112",
        "JX1400",
        "JX2217",
        "LB6810",
        "MFCJ985DW",
        "MFCL5700DW",
        "MFCL5800DW",
        "MFCL5900DW",
        "MFCL6700DW",
        "MFCL6900DW",
        "NQ1300",
        "NQ1400E",
        "NQ3500D",
        "NQ550",
        "NQ700",
        "NQ900",
        "NV990D",
        "PRS100",
        "PQ1500S",
        "PTD600",
        "PTE500VP",
        "PTP750W",
        "PT300",
        "QL700",
        "SC9500",
        "VM6200D",
        "VQ2400",
        "VQ3000",
        "XV8500D"
    ];

    for (var i = 0; i < codes.length; i++) {
        getProductusingID(codes[i]);
    }

}
var getProductusingID = function(productCode){
    // console.log(productImageMapinfo);
    // productResource.getProduct({productCode:productImageMapinfo.productCode}).then(function(response){
    productResource.getProduct({productCode:productCode}).then(function(response){
        consoleLog("|");
        // console.log(JSON.stringify(response));

        console.log("\t Got product object");
        updateProductImageInFrench(response);
    }).catch(function(error){
        console.log("No product found!  : "+productCode);
    });
}

var updateProductImageInFrench = function(productInfo){
    for (var i = 0; i < productInfo.productInCatalogs.length; i++) {
        if(productInfo.productInCatalogs[i].catalogId === 2){
            productInfo.productInCatalogs[i].content.productImages = [];
            for (var j = 0; j < productInfo.content.productImages.length; j++) {

                productInfo.productInCatalogs[i].content.productImages.push({
                    localeCode : 'fr-CA',
                    cmsId: productInfo.content.productImages[j].cmsId,
                    sequence: j
                });
            }
        }
    }
    console.log("\tUpdating");
    productResource.updateProduct(productInfo).then(function(response){
        console.log("\t\t Updated product");
    }).catch(function(error){
        console.log("Update Failed for : ");
        console.log(productImageMapinfo.productCode);
    });
}

// Lest start the script by calling the start program.
// start();
start2();
