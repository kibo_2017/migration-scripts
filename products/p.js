var apiContext      = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = 22834;
// apiContext.context.siteId = 33271;
apiContext.context.siteId = 33272;

// apiContext.context.tenant = 17149;
// apiContext.context.siteId = 26603;
// apiContext.context.siteId = 33272;
var productResource = require('mozu-node-sdk/clients/commerce/catalog/admin/product')(apiContext);

// console.log(apiContext.context);


var update = function(code,endlighsName,frenchName){
    productResource.getProduct({productCode:code}).then(function(responseBody){
        // viewName : category
        // console.log(JSON.stringify(responseBody));
        responseBody.content.productName = endlighsName;
        responseBody.productInCatalogs[0].content.productName = endlighsName;
        responseBody.productInCatalogs[0].productCode = responseBody.productCode;
        responseBody.productInCatalogs[1].content.productName = frenchName;
        responseBody.productInCatalogs[1].productCode = responseBody.productCode;
        productResource.updateProduct(responseBody).then(function(resp){
            console.log("update in master");
        }).catch(function(error){
            console.log(error);
        });
        productResource.updateProductInCatalog(responseBody.productInCatalogs[0]).then(function(resp){
            console.log("update in english");
        }).catch(function(error){
            console.log(error);
        });
        productResource.updateProductInCatalog(responseBody.productInCatalogs[1]).then(function(resp){
            console.log("update in french");
        }).catch(function(error){
            console.log(error);
        });
        console.log("Got new Data");
    }).catch(function(err){
        console.log(err);
        console.log("Error Happend");
    });
}


update('BCARE1IN','Brother Care Free','Brother Care Free');
update('BCARE2IN','Brother Care Gold','Brother Care Gold');
update('BCARE3IN','Brother Care Platinum','Brother Care Platinum');
