/**
 * Functionality to update the images URL in the KIBO admin panle
 * Following script will remove 'https://' and 'http://' from the product images
 * URL and will keep it just  '//'
**/
var apiContext      = require('mozu-node-sdk/clients/platform/application')();

var DEV_TENANT_ID = 17149;
var QA_TENANT_ID  = 17485;
var DEMO_TENANT_ID = 17406;
var UAT_TENANT_ID = 22834;

var apiContext      = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = 17406;
// apiContext.context.siteId = 27154;
var productResource = require('mozu-node-sdk/clients/commerce/catalog/admin/product')(apiContext);


var consoleLog = function(str,error){
    process.stdout.write(str+'');
}


var start = function(){
    getProduct();
    // getProduct1();
}

var getProduct1 = function(){
    productResource.getProduct({productCode:'QL570'}).then(function(resp){
        console.log(JSON.stringify(resp));
    }).catch(function(err){
        console.log(err);
    });
}

var getProduct = function(startIndex){
    productResource.getProducts({
        startIndex : startIndex ? startIndex:0,
        pageSize: 200
    }).then(function(resp){
        processProducts(resp);
    }).catch(function(error){
        console.log(error);
    });
}

var productToUpdate = [];
var productCodes = [];
var processProducts =  function(response){
    var isToUpdate = false;
    for (var i = 0; i < response.items.length; i++) {
        productCodes.push(response.items[i].productCode);
        isToUpdate = false;
        if(response.items[i].properties && response.items[i].properties.length>0){
            for (var j = 0; j < response.items[i].properties.length; j++) {
                if(response.items[i].properties[j].values && 
                    response.items[i].properties[j].values[0] &&
                    response.items[i].properties[j].values[0].value.indexOf &&
                    response.items[i].properties[j].values[0].value.indexOf('//')>-1){
                    console.log(response.items[i].properties[j].values[0].value);
                    var s = response.items[i].properties[j].values[0].value;
                    while(s.indexOf('http://')>-1){
                        s = s.replace('http://','//');
                    }
                    while(s.indexOf('https://')>-1){
                        s = s.replace('https://','//');
                    }
                    response.items[i].properties[j].values[0].value = s;
                    if(response.items[i].properties[j].values[0].content && response.items[i].properties[j].values[0].content.stringValue){
                        s = response.items[i].properties[j].values[0].content.stringValue;
                        while(s.indexOf('http://')>-1){
                            s = s.replace('http://','//');
                        }
                        while(s.indexOf('https://')>-1){
                            s = s.replace('https://','//');
                        }
                        response.items[i].properties[j].values[0].content.stringValue = s;
                    }
                    if(response.items[i].properties[j].values[0].localizedContent){
                        for (var k = 0; k < response.items[i].properties[j].values[0].localizedContent.length; k++) {
                            if(response.items[i].properties[j].values[0].localizedContent[k].stringValue){
                                s = response.items[i].properties[j].values[0].localizedContent[k].stringValue;
                                while(s.indexOf('http://')>-1){
                                    s = s.replace('http://','//');
                                }
                                while(s.indexOf('https://')>-1){
                                    s = s.replace('https://','//');
                                }
                                response.items[i].properties[j].values[0].localizedContent[k].stringValue = s;
                            }
                        }
                    }
                    isToUpdate = true;
                    console.log("\t"+response.items[i].properties[j].values[0].value);
                }
            }
        }
        if(isToUpdate){
            productToUpdate.push(response.items[i]);
        }
    }
    console.log("-----------------");
    console.log(response.totalCount);
    console.log(response.startIndex);
    console.log(response.pageSize);
    if(response.totalCount > response.startIndex+response.pageSize){
        console.log("Calling again!");
        console.log("\n");
        getProduct(response.startIndex+response.pageSize);
    }else{
        console.log("Update all selected products");
        console.log(productToUpdate.length);
        updateProduct(productToUpdate,0);
        // updateProduct(productCodes,0);
    }
}

var updateProduct = function(productToUpdate, startIndex){
    console.log(productToUpdate.length);
    console.log(startIndex);
    if(startIndex == productToUpdate.length ){
        console.log("Done!!");
    }else if(startIndex < productToUpdate.length && startIndex+50 < productToUpdate.length){
        updateProductByBatch(productToUpdate, startIndex, startIndex+50);
    }else{
        updateProductByBatch(productToUpdate, startIndex, productToUpdate.length);
    }
}

var updateProductByBatch = function(productToUpdate, start, end){
    var updatedCount = start;
    for (var i = start; i < end; i++) {
        // productResource.updateProductInCatalogs({productCode:productToUpdate[i]}).then(function(resp){
        productResource.updateProduct(productToUpdate[i]).then(function(resp){
            updatedCount = updatedCount+1;
            console.log('UPDATED!'+updatedCount);
            if(updatedCount == end){
                updateProduct(productToUpdate, end);
            }
            // console.log(JSON.stringify(resp));
        }).catch(function(err){
            console.log(err);
            console.log('ERRER!');
        });
    }
}


start();
