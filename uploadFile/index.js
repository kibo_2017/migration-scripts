var apiContext = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = 17149;
apiContext.context['master-catalog'] = 1;
var documentResource     = require('mozu-node-sdk/clients/content/documentlists/document')(apiContext);
var fs =  require('fs');

documentResource.createDocument({
    documentListName    : 'files@mozu',
    documentTypeFQN     : 'image@mozu',
    name                : 'test_upload_image2',
    contentMimeType     : 'image/jpeg'
}).then(function(response){
    var file = fs.readFileSync('./uploadFile/test_upload_image2.jpg');
    documentResource.updateDocumentContent({
        documentListName: "files@mozu",
        documentId: response.id
    }, {
        body: file,
        headers: {
            'Content-Type': 'image/jpg'
        }
    }).then((result)=>{
        console.log(result);
    }).catch((err)=>{
        console.error(err);
    });
}).catch(function(error){
    console.log(error);
});
