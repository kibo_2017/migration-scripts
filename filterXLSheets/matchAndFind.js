var csv = require("fast-csv");
var fs = require('fs');
var fileName = './customerIdmapping/logs_2/0_50000.csv';
var XLSX = require('xlsx');


var readExistingCustomerInfo =  function(){
    console.log("Reading all fileNames");
    var dir = './filterXLSheets/logs/';
    var EXISTING_INFO_FILE_READERS = [];
    var totalReaded = 0;
    fs.readdir(dir,function(err,fileNames){
        for (var i = 0; i < fileNames.length; i++) {
            var dataArr = [];
            csv.fromPath(dir+fileNames[i], {headers: true})
            .on("data", data => {
                dataArr.push(data);
            })
            .on("end", () => {
                totalReaded = totalReaded +1;
                EXISTING_INFO_FILE_READERS.push(dataArr);
                if(totalReaded == fileNames.length){
                    console.log("Completed reading CSV files in the folder.");
                    crossCheckWithGivenInfo(EXISTING_INFO_FILE_READERS);
                }
            });
        }
    });
}

var totalCount = 0;
var crossCheckWithGivenInfo =  function(EXISTING_INFO_FILE_READERS){
    var fileWriteStream = fileWriteStream = fs.createWriteStream('./filterXLSheets/'+'result_'+(new Date())+'.csv',{
        autoClose: true
    });
    fileWriteStream.write("KIBO_CUSTOMER_ID"+","+"SAP_BP_NUMBER"+","+"KIBO_ADDRESS_ID"+","+"SAP_ADDRESS_ID"+","+new Date());
    fileWriteStream.write("\n");
    var workbook = XLSX.readFile('./getCustomerInfo/BP_WITHEMAIL_NOMOZUID.XLSX');
    var worksheet = workbook.Sheets[workbook.SheetNames[0]];
    var xlsxData = XLSX.utils.sheet_to_json(worksheet, {header:1});
    console.log("Readed sheet shared by client : "+xlsxData.length);
    // for (var i = 0; i < xlsxData.length; i++) {
    // var i = 0;
    // while( i < xlsxData.length) {
    //
    //     if(xlsxData[i] && xlsxData[i][0]){
    //         var CHECKER =  require('../filterXLSheets/checker');
    //         CHECKER.isExistInSheets(xlsxData[i][0],EXISTING_INFO_FILE_READERS,fileWriteStream);
    //     }
    // }
    var referenceObject = createObjectFromArray(EXISTING_INFO_FILE_READERS);
    findItems(referenceObject,xlsxData,fileWriteStream);
}

var createObjectFromArray =  function(EXISTING_INFO_FILE_READERS){
    var object = {};
    EXISTING_INFO_FILE_READERS.find(function(EXISTING_INFO_FILE_READER){
        EXISTING_INFO_FILE_READER.find(function(x){
            if(object[x.SAP_BP_NUMBER.toLowerCase()]){
                object[x.SAP_BP_NUMBER.toLowerCase()].push(x);
            }else{
                object[x.SAP_BP_NUMBER.toLowerCase()] = [x];
            }
        });
    });
    return object;
}

var findItems =  function(referenceObject,xlsxData,fileWriteStream){
    for (var i = 0; i < xlsxData.length; i++) {
        if(xlsxData[i] && xlsxData[i][0]){
            if(referenceObject[xlsxData[i][0].toLowerCase()]){
                process.stdout.write("🍺");
                writer(referenceObject[xlsxData[i][0].toLowerCase()],fileWriteStream);
            }else{
                process.stdout.write("❤️");
            }
        }
    }
}

var writer = function(mappingInfo,fileWriteStream){
    totalCount =  totalCount + mappingInfo.length;
    mappingInfo.filter(function(x){
        fileWriteStream.write(x.KIBO_CUSTOMER_ID+","+x.SAP_BP_NUMBER+","+x.KIBO_ADDRESS_ID+","+x.SAP_ADDRESS_ID+","+new Date());
        fileWriteStream.write("\n");
    });
    console.log("");
    console.log(totalCount);
    console.log("");
}


readExistingCustomerInfo();
