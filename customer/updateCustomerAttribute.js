
var DEV_TENANT_ID = 17149;
var QA_TENANT_ID  = 17485;
var DEMO_TENANT_ID = 17406;
var UAT_TENANT_ID = 22834;

var apiContext      = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = UAT_TENANT_ID;
var customerResource = require('mozu-node-sdk/clients/commerce/customer/customerAccount')(apiContext);

var XLSX = require('xlsx');
var attribute_info = './customer/data_attribute.xlsx';

var updatedCount = 1, xlsxData;
var readCustomerAttribute = function(){
    var workbook = XLSX.readFile(attribute_info);
    var worksheet = workbook.Sheets[workbook.SheetNames[0]];
    xlsxData = XLSX.utils.sheet_to_json(worksheet, {header:1});
    inter();
}

var limit = 0;
var inter =  function(){
    // updatedCount = 0;
    console.log(xlsxData.length);
    limit = updatedCount+25;
    for (var i = updatedCount; i < limit && i < xlsxData.length; i++) {
        // [ '8890013695', '0', '0', '0', '0', '0', '0', '0', '0', 'OSVC' ]
        updateCustomerByExternalId(xlsxData[i]);
    }
}


var updateCustomerByExternalId = function(info){
    // get customer by external Id
    // var info = [ '8890013695', '1', '1', '1', '1', '0', '0', '0', '0', 'OSVC' ];
    customerResource.getAccounts({
        'filter':'externalId eq 8890013695'
    }).then(function(response){
        // console.log(JSON.stringify(response));
        console.log("_");
        if(response.items.length>0){
            var customer = response.items[0];
            updateCustomerAttribute(customer,info);
        }
    }).catch(function(error){
        console.log(error);
    });
}



var updateCustomerAttribute = function(customer,info){
    // console.log(info);
    customer['accountId'] = customer.id;
    customer.attributes.push({
        fullyQualifiedName : 'tenant~promo',
        values : [info[1]=='0'?false:true]
    });
    customer.attributes.push({
        fullyQualifiedName : 'tenant~bmg',
        values : [info[2]=='0'?false:true]
    });
    customer.attributes.push({
        fullyQualifiedName : 'tenant~had',
        values : [info[3]=='0'?false:true]
    });
    customer.attributes.push({
        fullyQualifiedName : 'tenant~brothercare1',
        values : [info[4]=='0'?false:true]
    });
    customer.attributes.push({
        fullyQualifiedName : 'tenant~brothercare2',
        values : [info[5]=='0'?false:true]
    });
    customer.attributes.push({
        fullyQualifiedName : 'tenant~brothercare3',
        values : [info[6]=='0'?false:true]
    });
    customer.attributes.push({
        fullyQualifiedName : 'tenant~news',
        values : [info[7]=='0'?false:true]
    });
    // console.log(customer);
    customerResource.updateAccount(customer).then(function(resp){
        // console.log(resp);

        updatedCount = updatedCount +1;
        console.log("|"+updatedCount);
        if(updatedCount == limit-1){
                inter();
        }

    }).catch(function(error){
        console.log(error);
        updatedCount = updatedCount +1;
        if(updatedCount == limit-1){
                inter();
        }
    });
}

// updateCustomerByExternalId();
readCustomerAttribute(1);
