
var DEV_TENANT_ID = 17149;
var QA_TENANT_ID  = 17485;
var DEMO_TENANT_ID = 17406;
var UAT_TENANT_ID = 22834;
var PROD_TENANT_ID = 21830;

var apiContext      = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = DEMO_TENANT_ID;
apiContext.context['master-catalog'] = 1;
var customerAttribute = require('mozu-node-sdk/clients/commerce/customer/attributedefinition/attribute')(apiContext);
var customerResource = require('mozu-node-sdk/clients/commerce/customer/customerAccount')(apiContext);

var consoleLog = function(str,error){
    process.stdout.write(str+'');
}


var start = function(){
    getCustomer();
    // getUserById();
}

var getUserById =  function(){
    let id = "635ab6bb68d749298626cbf13c03d42f";
    customerResource.getAccounts({
        filter:'userId  eq "635ab6bb68d749298626cbf13c03d42f"'
        // filter:'userId  eq "0c5a42bd81314f698d20b969154ce927"'
    }).then(function(response){
        console.log(response);
        // deleteAllCustomer(response);
    }).catch(function(error){
        console.log(error);
        consoleLog("OVER!");
    });
}

var getCustomer = function(startIndex){
    var apiContext      = require('mozu-node-sdk/clients/platform/application')();
    apiContext.context.tenant = PROD_TENANT_ID;
    apiContext.context['master-catalog'] = 2;
    var customerResource = require('mozu-node-sdk/clients/commerce/customer/customerAccount')(apiContext);
    customerResource.getAccounts({
        pageSize:500,
        responseFields : 'items(id)'
    }).then(function(response){
        // console.log(JSON.stringify(response));
        console.log("\n\n");
        console.log(response.totalCount);
        console.log("\n\n");
        deleteAllCustomer(response);
        // for (var i = 0; i < response.items.length; i++) {
        //     updateCustomer(response.items[i]);
        // }
    }).catch(function(error){
        console.log(error);
        consoleLog("OVER!");
    });
}
var updateCustomer =  function(resp){
    var apiContext      = require('mozu-node-sdk/clients/platform/application')();
    apiContext.context.tenant = DEV_TENANT_ID;
    apiContext.context['master-catalog'] = 1;
    var customerResource = require('mozu-node-sdk/clients/commerce/customer/customerAccount')(apiContext);

    var externalId = 1802803;
    var BROTHERCARE_LEVEL = "tenant~brothercarelevel";
    var brothercarelevel = Math.floor(Math.random() * 8) + 2;
    brothercarelevel = brothercarelevel+'';
    var isUpdated = false;
    for (var i = 0; i < resp.attributes.length; i++) {
        if(resp.attributes[i].fullyQualifiedName == BROTHERCARE_LEVEL){
            isUpdated = true;
            resp.attributes[i].values  = [brothercarelevel];
        }
    }
    if(!isUpdated){
        resp.attributes.push({
            fullyQualifiedName : BROTHERCARE_LEVEL,
            values : [brothercarelevel]
        });
    }
    resp.accountId = resp.id;
    upd(resp, brothercarelevel);
}

var upd =  function(resp, brothercarelevel){
    customerResource.updateAccount(resp).then(function(respr){
        console.log("updated : \t "+brothercarelevel);
        console.log("resp.accountId : \t"+resp.accountId);
    }).catch(function(errr){
        console.log(errr);
        console.log("errr");
    });
}


var deleteAllCustomer =  function(response){
    var apiContext      = require('mozu-node-sdk/clients/platform/application')();
    apiContext.context.tenant = PROD_TENANT_ID;
    apiContext.context['master-catalog'] = 2;
    var customerResource = require('mozu-node-sdk/clients/commerce/customer/customerAccount')(apiContext);
    if(response.items.length > 0){
        for (var i = 0; i < response.items.length; i++) {
            customerResource.deleteAccount({
                accountId:response.items[i].id
            }).then(function(){
                consoleLog("|");
            }).catch(function(){
                consoleLog("_");
            });
        }
        getCustomer();
    }else{
        consoleLog("OVER-OVER!");
    }

}

var updateAccount =  function(){
    var externalId = 1802803;
    customerResource.getAccount({accountId:2514}).then(function(resp){
        console.log(resp);
        resp.externalId = externalId;
        resp['accountId'] = 2514;
        customerResource.updateAccount(resp).then(function(respr){
            console.log(respr);
        }).catch(function(errr){
            console.log(errr);
        });
    }).catch(function(err){
        console.log(err);
    });
}

var addAccount = function(uuid){

    var data =[];

    for (var i = 0; i < 1; i++) {
        var uuid = i+'mmm';
        var info = {
            acceptsMarketing: true,
            companyOrOrganization: "stringz",
            emailAddress: uuid+"stringz@migrate.com",
            firstName: uuid+"zmigration",
            isActive: true,
            lastName: uuid+"zmigration",
            userName: uuid+"zmigration"
       };

       customerResource.addAccount(info).then(function(resp){
           consoleLog("◼︎ ");
        //    console.log(resp);
       }).catch(function(er){
           console.log(er);
       });
    }


}

var getCustomerAttribute = function(startIndex){
    customerAttribute.getAttributes({
        startIndex : 0,
        pageSize : 100
    }).then(function(resp){
        console.log(resp);

    }).catch(function(error){
        console.log("Error");
        console.log(error);
    });
}

start();
