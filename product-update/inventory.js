/**
 * Code to read dayta from xlsx and upload inventory
 **/

 var apiContext      = require('mozu-node-sdk/clients/platform/application')();
 apiContext.context.tenant = 22834;

var locationInventoryResource = require('mozu-node-sdk/clients/commerce/catalog/admin/products/locationInventory')(apiContext);

var LOCATION_CODE = 'L01';

var getLocationInventories = function(productCode){
    locationInventoryResource.getLocationInventories({
        'ProductCode': productCode
    }).then(function(response){
        console.log(response);
    }).catch(function(error){
        console.log(error);
    });
}

var XLSX = require('xlsx');
var readFromXLSXsheet = function(){
    var workbook = XLSX.readFile('./data.xlsx');
    console.log(workbook.SheetNames);
    var worksheet = workbook.Sheets[workbook.SheetNames[0]];
    // console.log(worksheet);
    var xlsxData = XLSX.utils.sheet_to_json(worksheet, {header:1});
    for (var i = 1; i < xlsxData.length; i++) {
        updateLocationInventory(xlsxData[i][0], parseFloat(xlsxData[i][xlsxData[i].length-1]));
    }
    // console.log(worksheet);
}


var count = 0;
var addLocationInventory =  function(pcode,inventory){
    var data =  {
        // productCode : pcode,
        // locationCode: LOCATION_CODE,
        // type : 'Absolute',
        // Value : inventory,
        "baseProductCode": pcode,
        "locationCode": LOCATION_CODE,
        "productCode": pcode,
        "stockAvailable": inventory,
        "stockOnBackOrder": 0,
        "stockOnHand": inventory
    };
    locationInventoryResource.addLocationInventory({
        ProductCode : pcode,
        performUpserts: true
    },{body:[data]}).then(function(response){

        // console.log("added : "+pcode);
        // console.log(response);
    }).catch(function(error){
        console.log(pcode+" ERROR!!");
        // console.log(error);
    });
}

var updateLocationInventory =  function(pcode,inventory){
    var data =  {
        productCode : pcode,
        locationCode: LOCATION_CODE,
        type : 'Absolute',
        Value : inventory
    };
    locationInventoryResource.updateLocationInventory({ProductCode : pcode},{body:[data]}).then(function(response){
        // console.log(response);
        // console.log("updated : "+pcode);
    }).catch(function(error){
        console.log("not updated : "+pcode);
        addLocationInventory(pcode,inventory);
    });
}

// addLocationInventory('1134D',123);
// addLocationInventory('122991052',123);
readFromXLSXsheet();
