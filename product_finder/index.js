var apiContext      = require('mozu-node-sdk/clients/platform/application')();
// Provide tenant id and master-catalog id
apiContext.context.tenant = 17149;
apiContext.context['master-catalog'] = 1;

var productResource = require('mozu-node-sdk/clients/commerce/catalog/admin/product')(apiContext);

// Provide all the attribute needed to be removed from the products
var attributesToDelete = ['tenant~showaddtocart'];

getProducts(0);

// Functionality to fetch the products from the KIBO tenant
// This will fetch 200 products from the given startIndex
// Response will be passed to filter products funciton
function getProducts(newStartIndex){
    productResource.getProducts({
        startIndex : newStartIndex,
        pageSize : 100
    }).then(function(response){
        filterProduct(response);
    }).catch(function(error){
        console.log(error);
    });

}

// Loop throggh each products in the response
// check if the product have the property given in the attributesToDelete
// Delete the property from the properties list
// If any update happend to product pass the product for update product Functionality
function filterProduct(response){
    console.log("\n\n");
    console.log(`${response.startIndex}  to  ${response.startIndex+response.pageSize}`);
    console.log("\n\n");

    response.items && response.items.filter(function(item){
        let isUpdated = false;
        item.properties && item.properties.filter(function(property,index){
            if(attributesToDelete.indexOf(property.attributeFQN)>-1){
                console.log(`DELETED FROM PRODUCT CODE : ${item.productCode} \t\t VALUE : ${property.values[0].value}`);
                item.properties.splice(index,1);
                isUpdated = true;
            }
        });
        if(isUpdated){
            updateProduct(item);
        }
    });
    if(response.startIndex+response.pageSize <  response.totalCount){
        getProducts(response.startIndex+response.pageSize);
    }else{
        console.log("\n\nEND");
    }

}

// Functionality to update the product in KIBO
function updateProduct(item){
    productResource.updateProduct(item).then(function(response){
        console.log(`updateProduct :  ${item.productCode}`);
    }).catch(function(error){
        console.log(error);
    });
}
