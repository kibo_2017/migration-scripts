var apiContext      = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = 18670;
apiContext.context.siteId = 28913;
var facetResource = require('mozu-node-sdk/clients/commerce/catalog/admin/facet')(apiContext);
var categoryResource = require('mozu-node-sdk/clients/commerce/catalog/admin/category')(apiContext);



var start = function(){
    // categoryResource.getChildCategories({categoryId:33}).then(function(resp){
    //     // console.log(JSON.stringify(resp));
    //     var responseCategory = resp.items;
        readFacetInfo('./facets/data.xlsx');
    // }).catch(function(err){
    //     console.log(err);
    // });
}


var addFacets =  function(){

}

var XLSX = require('xlsx');
var readFacetInfo =  function(csvFile){
    var workbook = XLSX.readFile(csvFile);
    var worksheet = workbook.Sheets[workbook.SheetNames[0]];
    var xlsxData = XLSX.utils.sheet_to_json(worksheet, {header:1});
    // console.log(JSON.stringify(xlsxData));
    console.log("\n\n");
    var facets = xlsxData[1];
    for (var i = 2; i < 4; i++) {//xlsxData.length
        // category name and some filter value is not null.
        console.log(xlsxData[i]);
        console.log("-----------");
        if(xlsxData[i].length > 1){
            // updateFacetForCategory(xlsxData[i],facets);
            // console.log(xlsxData[i]);
            associateCategoryFacet(xlsxData[i][0],xlsxData[i],facets);
        }
        // var rowObject =  {};
        // for (var j = 0; j < xlsxData[0].length; j++) {
        //     rowObject[xlsxData[0][j]] = xlsxData[i][j]?xlsxData[i][j]:'';
        // }
        // console.log(rowObject);
    }
}


var associateCategoryFacet =  function(categoryIds,facetsToBeConfigured, facets){
    console.log(categoryIds);
    console.log(facetsToBeConfigured);
    console.log(facets);
    console.log("-----------");
    for (var i = 1; i < facetsToBeConfigured.length; i++) {
        if(facetsToBeConfigured[i] == 1){
            var data =  getFacetConfigurationInfo(facets[i]);
            if(data){
                var categories = categoryIds.split(',');
                console.log(categories);
                for (var j = 0; j < categories.length; j++) {
                    data['CategoryId'] = categories[j];
                    facetResource.addFacet(data).then(function(resp){
                        console.log(resp);
                    }).catch(function(error){
                        console.log(error);
                    });
                }

            }
        }
    }
}

var getFacetConfigurationInfo =  function(facetInfo){
    var facetData = {
        categoryId: "",
        // facetId: "",
        facetType: "Value",
        isHidden: false,
        order: "int",
        source: {
            allowsRangeQuery: false,
            dataType: "String",
            id: "CategoryId",
            name: "name",
            type: "Attribute"
        },
        validity: {
            isValid: true,
            reasonCode: "Valid"
        },
        valueSortType: "ValuesAscending"
    };
    if(facetInfo && facetInfo.indexOf('tenant~')>-1){
        // Create facet information data based each attributeFQN value
        switch (facetInfo) {
            case "tenant~refurbished":
                facetData.order = 2;
                facetData.source.name = "Refurbished";
                facetData.source.id = "tenant~refurbished";
                return facetData;
                break;
            case "tenant~bestseller":
                // facetData.order = 3;
                facetData.source.id = "tenant~bestseller";
                return facetData;
                break;
            case "tenant~brothercare":
                facetData.order = 4;
                facetData.source.id = "tenant~brothercare";
                return facetData;
                break;
            case "tenant~duplex-printing":
                facetData.order = 5;
                facetData.source.name = "Duplex Printing";
                facetData.source.id = "tenant~duplex-printing";
                return facetData;
                break;
            default:
                return false;
        }
    }else{
        return false;
    }
}

var addFacetTemp =  function(){
    // var data =  getFacetConfigurationInfo('tenant~bestseller');
    // var data =  {
    //     categoryId: "",
    //     // facetId: "",
    //     facetType: "Value",
    //     isHidden: false,
    //     // order: 1,
    //     source: {
    //         allowsRangeQuery: false,
    //         dataType: "String",
    //         id: "tenant~bestseller",
    //         name: "BestSeller",
    //         type: "Attribute"
    //     },
    //     validity: {
    //         isValid: true,
    //         reasonCode: "Valid"
    //     },
    //     valueSortType: "AttributeDefinition"
    // };
    // data['CategoryId'] = 38;
    // data['categoryId'] = 38;
    var data =  {
        categoryId: 38,
        // facetId: "",
        // facetType: "Value",
        // isHidden: false,
        // order: 1,
        source: {
            // allowsRangeQuery: false,
            // dataType: "String",
            id: "tenant~brothercare",
            // name: "BestSeller",
            // type: "Attribute"
        },
        // validity: {
        //     isValid: true,
        //     reasonCode: "Valid"
        // },
        // valueSortType: "AttributeDefinition"
    };
    facetResource.addFacet(data).then(function(resp){
        console.log(resp);
    }).catch(function(error){
        console.log(error);
    });
}

addFacetTemp();
// start();
