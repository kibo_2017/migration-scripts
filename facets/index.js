
var apiContext      = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = 18670;
apiContext.context.siteId = 28913;
var facetResource = require('mozu-node-sdk/clients/commerce/catalog/admin/facet')(apiContext);
var categoryResource = require('mozu-node-sdk/clients/commerce/catalog/admin/category')(apiContext);

// bmg_ho_scanners_portable
// 271

var getCategoriesAndDeleteAssociatedFacets = function(){
    categoryResource.getCategories({pageSize:200}).then(function(resp){
        // console.log(resp);
        for (var i = 0; i < resp.items.length; i++) {

            facetResource.getFacetCategoryList({categoryId:resp.items[i].catalogId}).then(function(response){

                process.stdout.write(response.configured.length+ "  , ");
                for (var i = 0; i < response.configured.length; i++) {
                    console.log(response.configured[i].facetId);
                    facetResource.deleteFacetById(response.configured[i]).then(function(resp){
                        // console.log(resp);
                        // console.log("\n\n");
                    }).catch(function(error){
                        console.log(error);
                    })
                }
            }).catch(function(error){
                console.log("------");
                console.log(error);
            });

        }
    }).catch(function(er){
        console.log(er);
    });
}
// getCategoriesAndDeleteAssociatedFacets();

var getCategoryFacet = function(){
    facetResource.getFacetCategoryList({categoryId:232}).then(function(response){
        console.log(JSON.stringify(response));
        process.stdout.write(response.configured.length+ "  , ");
        // for (var i = 0; i < response.configured.length; i++) {
        //     console.log(response.configured[i].facetId);
        //     facetResource.deleteFacetById(response.configured[i]).then(function(resp){
        //         // console.log(resp);
        //         // console.log("\n\n");
        //     }).catch(function(error){
        //         console.log(error);
        //     })
        // }
    }).catch(function(error){
        console.log("------");
        console.log(error);
    });
}

var deleteFacetByID = function(id){
    console.log(id);
    // console.log("\n");
    facetResource.deleteFacetById({facetId:id}).then(function(resp){
            console.log(resp);
            // console.log("\n\n");
        }).catch(function(error){
            console.log(error);
        })
}

var updateFacet =  function(){
    var data = {
        "categoryId": 203,
        "facetId": 15,
        // "facetType": "range",
        // "isHidden": false,
        // "order": 1,
        // // "overrideFacetId": "int",
        // "rangeQueries": [],
        // "source": {
        //     "allowsRangeQuery": true,
        //     "dataType": "Number",
        //     "id": "tenant~rating",
        //     "name": "Rating",
        //     "type": "Attribute"
        // },
        // "validity": {
        //     "isValid": true,
        //     "reasonCode": "VALID"
        // },
        // "valueSortType": "ValuesAscending"
    };
    facetResource.updateFacet(data).then(function(resp){
        console.log(resp);
    }).catch(function(er){
        console.log(er);
    });
}

var getCategory = function(){
    categoryResource.getCategory({categoryId:232}).then(function(resp){
        console.log(JSON.stringify(resp));
    }).catch(function(resp){
        console.log(resp);
    });
}
var addCategory = function(){
    var data = {
          "catalogId": 234,
          "categoryCode": "bmg_ho_scanners_portable",
          "categoryType": "Static",
          "childCount": 0,
          "content": {
            "categoryImages": [
              {
                "cmsId": "dcba6e5d-cdf6-48b4-b8f5-f68efa6b019a",
              }
            ],
            "description": "Numérisation « sur le pouce ». La rencontre d’un client dans un café? La copie d’une ancienne photo que vous voulez donner à un ami? Pas de problème! Numérisez tout, où bon vous semble - aucun bureau requis! C'est la façon de faire maintenant.",
            "localeCode": "fr-CA",
            "metaTagDescription": "Vous rencontrez un client dans un café? Vous voulez envoyer une photo à un ami? Les scanners portables numérisent tout, n'importe où!",
            "metaTagKeywords": "",
            "metaTagTitle": "",
            "name": "Scanners portables",
            "pageTitle": "",
            "slug": "scanners-portables"
          },
          "id": 234,
          "isActive": true,
          "isDisplayed": true,
          "parentCategoryCode": "bmg_ho_scanners",
          "parentCategoryId": 227,
          "parentCategoryName": "Scanners",
          "parentIsActive": true,
          "productCount": 5,
          "sequence": 2,
          "incrementSequence": false,
          "useProvidedId": true
      };
    categoryResource.addCategory(data).then(function(resp){
        console.log(resp);
    }).catch(function(err){
        console.log(err);
    });
}

// for (var i = 0; i < 500; i++) {
//     deleteFacetByID(i);
// }
// addCategory();
// getCategory();
// getCategoryFacet(18);
// deleteFacetByID(15);
// deleteFacetByID(16);
