var PROD_TENANT_ID = 21830;
var apiContext      = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = PROD_TENANT_ID;
apiContext.context['master-catalog'] = 2;
var orderResource = require('mozu-node-sdk/clients/commerce/order')(apiContext);


var fs = require('fs');
var XLSX = require('xlsx');
function start(){
    fileWriteStream = fs.createWriteStream('./order/logs.csv',{
        autoClose: true
    });
    fileWriteStream.write("TIME","KIBO_ORDER_NUMBER","KIBO_ORDER_ID","KIBO_ORDER_TOTAL","PAYMETRIC_TOTAL","SAP_ORDER_ID","SAP_ORDER_TOTAL");
    fileWriteStream.write("\n");
}


var getOrder = function (startIndex){
    console.log(new Date());
    orderResource.getOrders({
        startIndex : startIndex,
        pageSize : 200
    }).then(function(resp){
        console.log(resp.startIndex+"+"+resp.pageSize+" < "+resp.totalCount);
        if(resp.startIndex+resp.pageSize < resp.totalCount){
            getOrder(resp.startIndex+resp.pageSize);
        }
    }).catch(function(err){
        console.log(err);
    });
}


getOrder(0);
