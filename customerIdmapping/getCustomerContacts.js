
var fs = require('fs');
var PROD_TENANT_ID = 21830;



var getCustomerInfo = function(fileWriteStream, startIndex, initialStartIndex, limitSize){
    var fileWriteStream = fileWriteStream;
    if(!fileWriteStream){
        fileWriteStream = fs.createWriteStream('./customerIdmapping/logs/'+initialStartIndex+"_"+limitSize+".csv",{
            autoClose: true
        });
        fileWriteStream.write("KIBO_CUSTOMER_ID,SAP_BP_NUMBER,KIBO_ADDRESS_ID,SAP_ADDRESS_ID,TIME");
        fileWriteStream.write("\n");
    }
    var apiContext      = require('mozu-node-sdk/clients/platform/application')();
    apiContext.context.tenant = PROD_TENANT_ID;
    apiContext.context['master-catalog'] = 2;
    var customerResource = require('mozu-node-sdk/clients/commerce/customer/customerAccount')(apiContext);
    customerResource.getAccounts({
        startIndex : startIndex,
        pageSize :  200,
        responseFields: 'items(contacts,externalId)'
    }).then(function(response){
        // console.log(JSON.stringify(response));
        // console.log("from "+response.startIndex+" to "+(response.startIndex+200)+" out of extracted "+response.totalCount);
        var GET_FILE_WRITER = require('../customerIdmapping/fileWriter');
        GET_FILE_WRITER.writeDataToFile(fileWriteStream, response,  startIndex, initialStartIndex, limitSize);
    }).catch(function(error){
        console.log("ERROR!!!! >>>>>>>>>>>>>>>> TRYING TO FETCH AGAIN: "+startIndex);
        getCustomerInfo(fileWriteStream, startIndex, initialStartIndex, limitSize);
    });
}


var GET_CUSTOMER_MODULAR_FUNCTION = {
    'getCustomerInfo' : getCustomerInfo
}
module.exports = GET_CUSTOMER_MODULAR_FUNCTION;
