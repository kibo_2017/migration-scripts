var fs = require('fs');
var XLSX = require('xlsx');
var csv = require("fast-csv");
var PROD_TENANT_ID = 21830;


var fileWriteStream = fs.createWriteStream('customerIdmapping/log.csv',{
    autoClose: true
});
var consoleLog = function(loger){
    fileWriteStream.write(new Date()+","+loger);
    fileWriteStream.write("\n");
    process.stdout.write('|');
}


var start =  function(){
    fileWriteStream.write("TIME,BP_NUMBER,KIBO_ID");
    fileWriteStream.write("\n");
    processFromKibo(0);
}


var processFromKibo =  function(startIndex){
    var apiContext      = require('mozu-node-sdk/clients/platform/application')();
    apiContext.context.tenant = PROD_TENANT_ID;
    apiContext.context['master-catalog'] = 2;
    var customerResource = require('mozu-node-sdk/clients/commerce/customer/customerAccount')(apiContext);

    customerResource.getAccounts({
        startIndex : startIndex,
        pageSize :  200,
        responseFields : 'items(externalId,id)'
    }).then(function(response){
        if(response.items.length>0){
            for (var i = 0; i < response.items.length; i++) {
                consoleLog(response.items[i].externalId+","+response.items[i].id);
            }
            console.log(response.startIndex + 200);
            processFromKibo(response.startIndex + 200);
        }
    }).catch(function(error){
        console.log(error);
    });

}


start();
