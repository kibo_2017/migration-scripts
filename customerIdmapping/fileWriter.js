
var writeDataToFile =  function(fileWriteStream, response,  startIndex, initialStartIndex, limitSize){
    for (var i = 0; i < response.items.length; i++) {
        for (var j = 0; j < response.items[i].contacts.length; j++) {
            fileWriteStream.write(response.items[i].id+","+response.items[i].externalId+","+response.items[i].contacts[j].id+","+response.items[i].contacts[j].address.address4+","+new Date());
            fileWriteStream.write("\n");
        }
    }
    // console.log((startIndex+200)+", "+initialStartIndex+", "+limitSize+"   >>>  NEXT CALL");
    process.stdout.write((startIndex+200)+" < "+initialStartIndex);
    process.stdout.write(" || ");
    if(startIndex+200 < (initialStartIndex+limitSize)){
        var GET_CUSTOMER_MODULAR_FUNCTION =  require('../customerIdmapping/getCustomerContacts');
        GET_CUSTOMER_MODULAR_FUNCTION.getCustomerInfo(fileWriteStream, startIndex+200, initialStartIndex, limitSize);
    }else{
        console.log("Completed :: "+initialStartIndex+" to  "+(initialStartIndex+limitSize));
    }
}



var GET_FILE_WRITER = {
    'writeDataToFile' : writeDataToFile
}
module.exports = GET_FILE_WRITER;
