var apiContext              = require('mozu-node-sdk/clients/platform/application')();
var documentListResource    = require('mozu-node-sdk/clients/content/documentList')(apiContext);
var documentTypeResource    = require('mozu-node-sdk/clients/content/documentType')(apiContext);
var EntityListResource = require('mozu-node-sdk/clients/platform/entityList')(apiContext);


var EntityListStructure = {
    contextLevel    : "Tenant",
    idProperty: {
       dataType: "string",
       propertyName: "id"
    },

    isLocaleSpecific                : false,
    isSandboxDataCloningSupported   : true,
    isShopperSpecific               : false,
    isVisibleInStorefront           : true,
    useSystemAssignedId             : false,
    name        : 'product_group',
    nameSpace   : "Brother",
    tenantId    : 22834,
    usages      : ["entityManager"],
    views       : []
};


EntityListResource.createEntityList(EntityListStructure).then(function(response){
    console.log(" 🍺   🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺 ");
    console.log(JSON.stringify(response));
    console.log(" 🍺   🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺  🍺 ");
}).catch(function(error){
    console.log(error);
});
