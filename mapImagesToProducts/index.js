var fs = require('fs');
var XLSX = require('xlsx');
var csv = require("fast-csv");
var UAT_TENANT_ID = 22834;

var fileWriteStream ;


function start(){
    fileWriteStream = fs.createWriteStream('./mapImagesToProducts/logs.log',{
        autoClose: true
    });
    fileWriteStream.write(new Date() +"\t: START LOGGING");
    fileWriteStream.write("\n");
    fileWriteStream.write("-----------------------------------------------------");
    fileWriteStream.write("\n");
    readFromXLSXsheet();
}

function consoleLog(message){
    fileWriteStream.write(new Date() +"\t: "+message);
    fileWriteStream.write("\n");
}
var xlsxData;
var readFromXLSXsheet = function(){
    var workbook = XLSX.readFile('./mapImagesToProducts/mapping_info.xlsx');
    console.log(workbook.SheetNames);
    var worksheet = workbook.Sheets[workbook.SheetNames[1]];
    xlsxData = XLSX.utils.sheet_to_json(worksheet, {header:1});
    createMap(xlsxData);
}

var dataArray = [], logs = [];
var createMap  = function(xlsxData){
    var map = {};
    for (var i = 0; i < xlsxData.length; i++) {
        if(!map[xlsxData[i][2]]){
            map[xlsxData[i][2]] = [];
        }
        map[xlsxData[i][2]].push(xlsxData[i][1]);
    }
    for(var key in map){
        dataArray.push({
            key : key,
            value : map[key]
        });
    }


    console.log(JSON.stringify(dataArray));
    console.log("\n\n\n\n");
    updateInfo(1);
}



var updateInfo =  function(index){
    if(dataArray[index]){
        updateProductImage(dataArray[index], index);
    }else {
        logs.push({
            time : new Date(),
            productCode : "",
            CMSID : "",
            STATUS : ""
        });
        console.log("DONE");
        logs.push({
            time : new Date(),
            productCode : "DONE",
            CMSID : "DONE",
            STATUS :  "COMPLETED"
        });
        createLog(logs);
    }

}

var updateProductImage =  function(data, startIndex){
    var apiContext      = require('mozu-node-sdk/clients/platform/application')();
    apiContext.context.tenant = UAT_TENANT_ID;
    var productResource = require('mozu-node-sdk/clients/commerce/catalog/admin/product')(apiContext);
    // consoleLog(productCode+" ("+cmsId+")");
    // Get productCode
    productResource.getProduct({
        productCode : data.key
    }).then(function(resp){
        console.log("Is exist");

        var i = 0, isUpdated = false;
        var existingIds = [];
        if(resp.content.productImages && resp.content.productImages.length > 0){
            resp.content.productImages.filter(function(item){ existingIds.push(item.cmsId); });
        }else {
            resp.content.productImages = [];
        }
        for (i = 0; i < data.value.length; i++) {
            if(existingIds.indexOf(data.value[i]) == -1){
                isUpdated =  true;
                resp.content.productImages.push({
                    localeCode : 'en-CA',
                    cmsId : data.value[i],
                    sequence : resp.content.productImages.length == 0 ? 1: resp.content.productImages.length+1
                });

                logs.push({
                    time : new Date(),
                    productCode : data.key,
                    CMSID : data.value[i],
                    STATUS : "UPDATED MASTER"
                });
            }else{
                logs.push({
                    time : new Date(),
                    productCode : data.key,
                    CMSID : data.value[i],
                    STATUS : "NOT UPDATED MASTER"
                });
            }
        }

        existingIds = [];
        if(resp.productInCatalogs[0].content.productImages && resp.productInCatalogs[0].content.productImages.length>0){
            resp.productInCatalogs[0].content.productImages.filter(function(item){ existingIds.push(item.cmsId); });
        }else{
            resp.productInCatalogs[0].content.productImages = [];
        }
        for (i = 0; i < data.value.length; i++) {
            if(existingIds.indexOf(data.value[i]) == -1){
                isUpdated =  true;
                resp.productInCatalogs[0].content.productImages.push({
                    localeCode : resp.productInCatalogs[0].content.localeCode,
                    cmsId : data.value[i],
                    sequence : resp.productInCatalogs[0].content.productImages.length == 0 ? 1: resp.productInCatalogs[0].content.productImages.length+1
                });

                logs.push({
                    time : new Date(),
                    productCode : data.key,
                    CMSID : data.value[i],
                    STATUS : "UPDATED -"+resp.productInCatalogs[0].content.localeCode
                });
            }else{
                logs.push({
                    time : new Date(),
                    productCode : data.key,
                    CMSID : data.value[i],
                    STATUS : "NO NEED TO UPDATED -"+resp.productInCatalogs[0].content.localeCode
                });
            }
        }


        existingIds = [];
        if(resp.productInCatalogs[1].content.productImages && resp.productInCatalogs[1].content.productImages.length>0){
            resp.productInCatalogs[1].content.productImages.filter(function(item){ existingIds.push(item.cmsId); });
        }else {
            resp.productInCatalogs[1].content.productImages = [];
        }
        for (i = 0; i < data.value.length; i++) {
            if(existingIds.indexOf(data.value[i]) == -1){
                isUpdated =  true;
                resp.productInCatalogs[1].content.productImages.push({
                    localeCode : resp.productInCatalogs[1].content.localeCode,
                    cmsId : data.value[i],
                    sequence : resp.productInCatalogs[1].content.productImages.length == 0 ? 1: resp.productInCatalogs[1].content.productImages.length+1
                });

                logs.push({
                    time : new Date(),
                    productCode : data.key,
                    CMSID : data.value[i],
                    STATUS : "UPDATED -"+resp.productInCatalogs[1].content.localeCode
                });
            }else{
                logs.push({
                    time : new Date(),
                    productCode : data.key,
                    CMSID : data.value[i],
                    STATUS : "NO NEED TO UPDATED "+resp.productInCatalogs[1].content.localeCode
                });
            }
        }

        if(isUpdated){
            console.log(data.key+" \t "+"\t UPDATED");
            productResource.updateProduct(resp).then(function(response){
                console.log(data.key+"\t UPDATED");
                console.log("UPDATED");
                logs.push({
                    time : new Date(),
                    productCode : data.key,
                    CMSID : "ALL",
                    STATUS : "UPDATED"
                });
                logs.push({
                    time : new Date(),
                    productCode : "",
                    CMSID : "",
                    STATUS : ""
                });
                callBack(startIndex+1);
            }).catch(function(error){
                console.log(data.key+"\t UPDATE FAILED :" +error.message);
                console.log("UPDATE FAILED");
                logs.push({
                    time : new Date(),
                    productCode : data.key,
                    CMSID : "ALL",
                    STATUS : "UPDATE FAILED"
                });
                logs.push({
                    time : new Date(),
                    productCode : "",
                    CMSID : "",
                    STATUS : ""
                });
                callBack(startIndex+1);
            });
        }else{
            for (var i = 0; i < data.value.length; i++) {
                logs.push({
                    time : new Date(),
                    productCode : data.key,
                    CMSID : data.value[i],
                    STATUS : "NO NEED TO UPDATE"
                });
            }
            logs.push({
                time : new Date(),
                productCode : "",
                CMSID : "",
                STATUS : ""
            });
            callBack(startIndex+1);
            console.log("NO NEED TO UPDATED");
        }

    }).catch(function(error){

        console.log(error);

        for (var i = 0; i < data.value.length; i++) {
            logs.push({
                time : new Date(),
                productCode : data.key,
                CMSID : data.value[i],
                STATUS : "PRODUCT NOT FOUND",
                ERROR : error.message
            });
        }
        console.log("PRODUCT NOT FOUND");
        callBack(startIndex+1);
    });
}


var createLog =  function(allData){
    var ws = fs.createWriteStream("./mapImagesToProducts/log.csv");
    csv.write(allData, {headers: true}).pipe(ws);
}


var callBack =  function(startIndex){
    updateInfo(startIndex);
}


start();
