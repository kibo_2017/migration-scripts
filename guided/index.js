var DEV_TENANT_ID = 17149;
var QA_TENANT_ID  = 17485;
var DEMO_TENANT_ID = 17406;
var UAT_TENANT_ID = 22834;
var PROD_TENANT_ID = 22834;

var apiContext = require('mozu-node-sdk/clients/platform/application')();
apiContext = UAT_TENANT_ID;

var ENTITY_LIST_FULLNAME = "content_item_info_list_1@brother";

var entityMainEnglish = "7c9096c0-083c-11e7-8036-1b87c38fab97";
var entityMainFrench = "7c9096c1-083c-11e7-8036-1b87c38fab97";

var entityBmgEnglish = "a9acce60-2887-11e7-8036-1b87c38fab97";
var entityBmgFrench = "a9acce61-2887-11e7-8036-1b87c38fab97";

var entityHadEnglish = "6adf3860-48af-11e7-8e74-99a9f5f007b9";
var entityHadFrench = "60d901e1-4844-11e7-8036-1b87c38fab97";


// console.log(JSON.stringify(customerAccountResource));

var getCmaEntity = function(cmaEntityId){
	var entityResource = require('mozu-node-sdk/clients/platform/entitylists/entity')(apiContext);
	var requestData = {
		entityListFullName: ENTITY_LIST_FULLNAME ,
		id:cmaEntityId
	};
	entityResource.getEntity(requestData).then(function (entity) {
        console.log(JSON.stringify(entity));
		console.log("data got it");
		// getUpdateGuidedSellingEntity(entityBmgEnglish,entity.data.config.model.tree[0].children[0]);
		// getUpdateGuidedSellingEntity(entityHadEnglish,entity.data.config.model.tree[0].children[1]);
	}).catch(function(error){
		console.log(error);
	});
};

var getUpdateGuidedSellingEntity = function(id, tree){
	var entityResource = require('mozu-node-sdk/clients/platform/entitylists/entity')(apiContext);
	var requestData = {
		entityListFullName: ENTITY_LIST_FULLNAME ,
		id:id
	};
	entityResource.getEntity(requestData).then(function (entity) {
		console.log(id + "i got data");
		entity.data = {
			config : {
				model : {
					tree : []
				}
			}
		}
		entity.data.config.model.tree[0] = tree;
		updateGuidedSellingEntity(entity);
		// console.log(id);
		// console.log(JSON.stringify(entity));
		// console.log("\n");
	}).catch(function(error){
		console.log(error);
	});
};

var updateGuidedSellingEntity = function(entity){
	var entityResource = require('mozu-node-sdk/clients/platform/entitylists/entity')(apiContext);
	entityResource.updateEntity(entity).then(function(updated){
		console.log(entity.id);
		console.log(JSON.stringify(updated));
	}).catch(function(error){
		console.log(error);
	});
};


// getCmaEntity("7c9096c0-083c-11e7-8036-1b87c38fab97"); //>> English
getCmaEntity("7c9096c1-083c-11e7-8036-1b87c38fab97"); //>> French
// getCmaEntity(entityFrench);
