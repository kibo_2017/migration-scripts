
var DEV_TENANT_ID = 17149;
var QA_TENANT_ID  = 17485;
var DEMO_TENANT_ID = 17406;
var UAT_TENANT_ID = 22834;

var apiContext      = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = UAT_TENANT_ID;
// apiContext.context.siteId = 28913;
var categoryResource = require('mozu-node-sdk/clients/commerce/catalog/admin/category')(apiContext);


var XLSX = require('xlsx');
var csvFile = './category/data.xlsx';


 var consoleLog = function(str,error){
     process.stdout.write(str+'');
 }
 var mapImageToOneObject = function(CateogryImageNameMapping){
     var mappingInfo =  [], isAdded =false;
     for (var i = 0; i < CateogryImageNameMapping.length; i++) {
         isAdded = false;
         for (var j = 0; j < mappingInfo.length; j++) {
            if(CateogryImageNameMapping[i].category_code == mappingInfo[j].category_code){
                isAdded = true;
                mappingInfo[j].imageNames.push(CateogryImageNameMapping[i].category_image);
            }
         }
         if(!isAdded){
             mappingInfo.push({
                category_code : CateogryImageNameMapping[i].category_code,
                imageNames : [CateogryImageNameMapping[i].category_image]
             });
         }
     }
     return mappingInfo;
 }

 // Functionality to read data from the file.
 var CateogryImageNameMapping = [];
 var readCategoryImageMappingInformationFromFile =  function(){
     try {
         // let open file, make sure the path is correct
         var workbook = XLSX.readFile('./category/data.xlsx');
         // As we know our data is reside in firt sheet, lets open that
         var worksheet = workbook.Sheets[workbook.SheetNames[1]];
         // Convert data into a matrix strucutre ;), the best way to process
         // multi dimentional data! My Grandpa told me this.
         var xlsxData = XLSX.utils.sheet_to_json(worksheet, {header:1});
         // first row will be the properties as we know its a xlsx data :P


         for (var i = 1; i < xlsxData.length; i++) {
             // Convert each row item into and object with proper mapping
             for (var i = 0; i < xlsxData.length; i++) {
                 CateogryImageNameMapping.push(
                     {
                         category_code : xlsxData[i][0],
                         category_image: xlsxData[i][2]?xlsxData[i][2].split('.')[0]:''
                     }
                 );
             }
         }
        //  console.log(JSON.stringify(CateogryImageNameMapping));
         readImageToCMSIDmappingInfromationFromFile(CateogryImageNameMapping);
     } catch (e) {
         console.log(e);
     }
 }

 var readImageToCMSIDmappingInfromationFromFile =  function(CateogryImageNameMapping){
     // let open file, make sure the path is correct
     var workbook = XLSX.readFile('./category/img_mapping.xlsx');
     // As we know our data is reside in firt sheet, lets open that
     var worksheet = workbook.Sheets[workbook.SheetNames[0]];
     // Convert data into a matrix strucutre ;), the best way to process
     // multi dimentional data! My Grandpa told me this.
     var xlsxData = XLSX.utils.sheet_to_json(worksheet, {header:1});
     // first row will be the properties as we know its a xlsx data :P

     var ImageToCMSIDmapping = [];
     for (var i = 1; i < xlsxData.length; i++) {
        //  console.log(xlsxData[i]);
         ImageToCMSIDmapping.push({
             cmsid : xlsxData[i][1],
             category_image: xlsxData[i][0]?xlsxData[i][0].split('.')[0]:''
         });
     }
     mapCMSIDtoCategoryImage(CateogryImageNameMapping,ImageToCMSIDmapping);
 }


var mapCMSIDtoCategoryImage = function(CateogryImageNameMapping,ImageToCMSIDmapping){
    for (var i = 0; i < ImageToCMSIDmapping.length; i++) {
        for (var j = 0; j < CateogryImageNameMapping.length; j++) {
            if(ImageToCMSIDmapping[i].category_image == CateogryImageNameMapping[j].category_image){
                if(!CateogryImageNameMapping[j].cmsids){
                    CateogryImageNameMapping[j].cmsids = []
                }
                CateogryImageNameMapping[j].cmsids.push(ImageToCMSIDmapping[i].cmsid);
            }
        }
    }
    // console.log(JSON.stringify(CateogryImageNameMapping));
    updateGetCategory(CateogryImageNameMapping)
}

var updateGetCategory =  function(CateogryImageNameMapping){
    // console.log(JSON.stringify(CateogryImageNameMapping));
    for (var i = 0; i < CateogryImageNameMapping.length; i++) {
    // for (var i = 0; i < 2; i++) {
        // getCategoryAndUpdate(CateogryImageNameMapping[i]);
        coreProcessor(CateogryImageNameMapping[i].category_code, CateogryImageNameMapping[i].cmsids);
    }
}


var getCategoryAndUpdate = function(CateogryImageNameMappingitem){
    if(CateogryImageNameMappingitem.cmsids && CateogryImageNameMappingitem.cmsids.length>0){
        categoryResource.getCategories({filter:'categoryCode eq '+CateogryImageNameMappingitem.category_code}).then(function(resp){
            // console.log(JSON.stringify(resp));
            for (var i = 0; i < resp.items.length; i++) {
                categoryResource.getCategory({categoryId:resp.items[i].id}).then(function(item){
                    updateCategory(item,CateogryImageNameMappingitem);
                }).catch(function(){
                    console.log("No way now category is not there!");
                });

            }
        }).catch(function(err){
            console.log("no category found");
            console.log(err);
        });
    }else{
        console.log("No cmsid : ");
    }
}

var updateCategory = function(item,CateogryImageNameMappingitem){
    // console.log(CateogryImageNameMappingitem);
    item.content.categoryImages = [];
    for (var i = 0; CateogryImageNameMappingitem.cmsids && i < CateogryImageNameMappingitem.cmsids.length; i++) {
        item.content.categoryImages.push({
            cmsid : CateogryImageNameMappingitem.cmsids[i],
            sequence: 0,
            localeCode: item.content.localeCode
        });
    }
    item['categoryId']  = item.id;
    item['cascadeVisibility']  = true;
    var apiContext2      = require('mozu-node-sdk/clients/platform/application')();
    apiContext2.context.catalogId = item.content.localeCode == 'fr-CA'? 1 : 2;
    // apiContext2.context.site = item.content.localeCode == 'fr-CA'? 33272 : 33271;
    var categoryResourceSite = require('mozu-node-sdk/clients/commerce/catalog/admin/category')(apiContext2);
    // console.log(apiContext2);
    categoryResourceSite.updateCategory(item).then(function(resp){
        console.log("--");
        // console.log(resp);
        console.log("\n\n");
    }).catch(function(err){
        console.log("++Update Error++");
        // console.log(err);
        console.log(CateogryImageNameMappingitem);
    });
}

var coreProcessor =  function(categoryCode,cmsids){
    categoryResource.getCategories({filter:'categoryCode eq '+categoryCode}).then(function(resp){
        console.log("Got category");
        for (var i = 0; i < resp.items.length; i++) {
            resp.items[i].content.categoryImages = [];

            for (var j = 0; j < cmsids.length; j++) {

                resp.items[i].content.categoryImages.push({
                    cmsid: cmsids[j],
                    sequence: 0,
                    localeCode: resp.items[i].content.localeCode
                });
            }
            resp.items[i]['categoryId']  = resp.items[i].id;
            resp.items[i]['cascadeVisibility']  = true;
            apiContext.context.catalog = resp.items[i].content.localeCode == 'fr-CA'? 2 : 1;
            // apiContext.context.catalog = resp.items[i].content.localeCode == 'fr-CA'? 26005 : 26003;
            var categoryResourceSite = require('mozu-node-sdk/clients/commerce/catalog/admin/category')(apiContext);

            categoryResourceSite.updateCategory(resp.items[i]).then(function(resp){
                console.log("update : "+categoryCode);
                // console.log(resp);
                console.log("\n");
            }).catch(function(err){
                console.log("++Update Error++ : "+categoryCode);
                // console.log(err);
            });
        }

    }).catch(function(err){
        console.log("no category found"+categoryCode);
        // console.log(err);
    });
}

// categoryResource.getCategories({filter:'categoryCode eq bmg_ho_mfc'}).then(function(resp){
//     // console.log(JSON.stringify(resp));
//     console.log("Got category");
//     // console.log(JSON.stringify(resp.items));
//     // categoryResource.getCategory({categoryId:resp.items[0].id}).then(function(resp){
//     //     console.log("Got category by id");
//     for (var i = 0; i < resp.items.length; i++) {
//         resp.items[i].content.categoryImages = [];
//         resp.items[i].content.categoryImages.push({
//             cmsid:'f8021d03-73e6-401c-80d3-5dab880c86be',
//             sequence: 0,
//             localeCode:resp.items[i].content.localeCode
//         });
//         resp.items[i]['categoryId']  = resp.items[i].id;
//         resp.items[i]['cascadeVisibility']  = true;
//         // console.log(resp.items[i]);
//
//         // console.log(apiContext);
//
//         apiContext.context.catalog = resp.items[i].content.localeCode == 'fr-CA'? 4 : 2;
//         // apiContext.context.catalog = resp.items[i].content.localeCode == 'fr-CA'? 26005 : 26003;
//         var categoryResourceSite = require('mozu-node-sdk/clients/commerce/catalog/admin/category')(apiContext);
//
//         categoryResourceSite.updateCategory(resp.items[i]).then(function(resp){
//             console.log("update");
//             console.log("--");
//             console.log(resp);
//             console.log("\n\n");
//         }).catch(function(err){
//             console.log("++Update Error++");
//             console.log(err);
//         });
//     }
//
// }).catch(function(err){
//     console.log("no category found");
//     console.log(err);
// });

readCategoryImageMappingInformationFromFile();
// updateGetCategory();
