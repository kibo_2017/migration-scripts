require(["modules/jquery-mozu", "underscore", "hyprlive", "modules/backbone-mozu", "modules/cart-monitor", 'modules/models-cart', "modules/models-product", "modules/views-productimages",  "modules/api", "modules/models-faceting","modules/product-custom", 'modules/iframexhr', "modules/cart-sharing", "modules/jquery-dateinput-localized","shim!vendor/owl-carousel/owl.new.carousel.min[jQuery=jquery]"], function ($, _, Hypr, Backbone, CartMonitor, CartModels, ProductModels, ProductImageViews, api, FacetModels,productCustomActions, IFrameXmlHttpRequest, CartShare) {

    var AddedToCartView = Backbone.MozuView.extend({
		templateName: 'modules/product/added-product-page2',
        initialize:function(){
            //This will be used to set silver plan to model variable.
            var me = this,
                cartItem = me.model.toJSON().data,
                BCSilverCode = Hypr.getThemeSetting('bcInSilverCode'),
                url = "/api/commerce/catalog/storefront/products/"+BCSilverCode,
                options = cartItem.product.options,
                isBCPlanAvailable = options.length > 0 ? _.findWhere(options,{attributeFQN:Hypr.getThemeSetting('attributesFQN').bcProductFQN}) : 0,
                BCPlanValue = isBCPlanAvailable ? isBCPlanAvailable.value : 0 ,
                featureHtml = '';

            if(BCPlanValue === Hypr.getThemeSetting('bcInBronzeCode')){   // BCPlanValue === 0  || 
                api.request('get',url).then(function(BCSilverProduct){
                    var attribute = Hypr.getThemeSetting('attributesFQN'),
                    replaceExchange = _.findWhere(BCSilverProduct.properties,{attributeFQN:attribute.replacementExchange}),
                    exclContent = _.findWhere(BCSilverProduct.properties,{attributeFQN:attribute.exclusiveContent}),
                    freeShip = _.findWhere(BCSilverProduct.properties,{attributeFQN:attribute.freeShipping}),
                    extendWarranty = _.findWhere(BCSilverProduct.properties,{attributeFQN:attribute.extendedWarranty}),
                    support = _.findWhere(BCSilverProduct.properties,{attributeFQN:attribute.customerSupport}),
                    savingsPass = _.findWhere(BCSilverProduct.properties,{attributeFQN:attribute.suppliesSavingsPass}),
                    tradeInAllowance = _.findWhere(BCSilverProduct.properties,{attributeFQN:attribute.tradeInAllowance});

                    featureHtml += '<div class="col-1">';
                    if(replaceExchange && replaceExchange.values[0].value){
                        featureHtml += '<p>'+Hypr.getLabel('feature1')+'</p>';
                    }
                    if(exclContent && exclContent.values[0].value){
                        featureHtml += '<p>'+Hypr.getLabel('feature2')+'</p>';
                    }
                    if(freeShip && freeShip.values[0].value){
                        featureHtml += '<p>'+freeShip.values[0].value+'</p>';
                    }
                    if(extendWarranty && extendWarranty.values[0].value){
                        featureHtml += '<p>'+extendWarranty.values[0].value+'</p>';
                    }
                    featureHtml += '</div>';

                    featureHtml += '<div class="col-2">';
                    if(support && support.values[0].value){
                        featureHtml += '<p>'+support.values[0].value+'</p>';
                    }
                    if(savingsPass && savingsPass.values[0].value){
                        featureHtml += '<p>'+savingsPass.values[0].value+'</p>';
                    }
                    if(tradeInAllowance && tradeInAllowance.values[0].value){
                        featureHtml += '<p>'+Hypr.getLabel('feature7')+'</p>';
                    }
                    featureHtml += '</div>';
                    $('div.brother-care-features').html(featureHtml);
                });
            }
        },
	 	render:function(){
            var me = this;
            Backbone.MozuView.prototype.render.apply(this);
        },

        isExistSilverPlanWithMarketingOption : false,
        silverPlanProductIds : [],

		upgradeSilver:function(e){
            // Mandatory check box for terms and conditions
            if(!$('#agreeToTerms').prop('checked')){
                return false;
            }
            $('.loader-wrapper').show();
            //
            var me = this,
                productData = me.model.get('data'),
                id = productData.id,
                mainProductCode = me.model.get('data').product.productCode,
                // total quantity in the cart - for main product
                quantity = productData.quantity,
                cartitemQty = quantity,
                // newly added quantity : TODO : Remove the check for addedQuantity
                addedQuantity = productData.addedQuantity? productData.addedQuantity: 1,
                updatedQty = cartitemQty - addedQuantity,
                existingPlan = _.findWhere(productData.product.options, {attributeFQN: Hypr.getThemeSetting('attributesFQN').bcProductFQN}),
                oldPlanCode = existingPlan.value,
                url = "/api/commerce/carts/current/items/" + id;
                // already prduct exist in the cart
                api.get('cart').then(function(cart){
                    for (var i = 0; i < cart.data.items.length; i++) {
                        // Getting each silver product in cart
                        if(cart.data.items[i].product.productCode == Hypr.getThemeSetting('bcInSilverCode')){
                            me.silverPlanProductIds.push(cart.data.items[i]);
                            var temp_option = _.findWhere(cart.data.items[i].product.options, {attributeFQN: Hypr.getThemeSetting('attributesFQN').marketingOptin});
                            me.isExistSilverPlanWithMarketingOption = me.isExistSilverPlanWithMarketingOption || temp_option?temp_option.value:false;
                        }
                    }

                    if(updatedQty > 0){
                        var newPlanCode = Hypr.getThemeSetting('bcInSilverCode');
                        productData.quantity = updatedQty;
                        /*  api call to find cartitem for existing BC plan and update quantity ().Reduce quantity */
                            var existingBCPlan = _.find(cart.data.items,function(item){ return item.product.productCode == oldPlanCode;});
                                existingBCPlan.quantity = updatedQty;

                            api.request('put', url, productData).then(function(cartitem){
                                api.request('put', "/api/commerce/carts/current/items/"+existingBCPlan.id, existingBCPlan).then(function(BCPlan){
                                    console.log('cart item & BC plan updated.');
                                    me.addNewBaseProduct(productData.product,addedQuantity);
                                    me.addSilverPlanProduct(addedQuantity);
                                });
                            });


                    }else{
                        var oldPlanOBJ = _.find(cart.data.items, function(item){return item.product.productCode == oldPlanCode;});
                        existingPlan.value = Hypr.getThemeSetting('bcInSilverCode');
                        // Update exisitng base product option
                        api.request('put', url, productData).then(function(res) {
                            // Delete exisitng bronze product added from PDP - for the base product
                            api.request('delete', '/api/commerce/carts/current/items/' + oldPlanOBJ.id, {}).then(function(res){
                                me.baseProductAdded = true;
                                me.addSilverPlanProduct(addedQuantity);
                            });
                        });
                    }
                });
        },

        silverProductAdded :false,
        addSilverPlanProduct: function(quantity){
            var me = this;
            var marketingOptin = $('#offers').prop('checked'); // selected marketin option
            // In cart some silver product marketing option is selected
            if(me.isExistSilverPlanWithMarketingOption){
                me.apiCallToAddSilverProduct(quantity,true,me.doCartShare);
            }else{
                if(marketingOptin){
                    // Add seilver prioduct and update cart silver product marketing options
                    me.apiCallToAddSilverProduct(quantity,true,me.updateCartSilverProductsmarketingOptions);
                }else{
                    me.apiCallToAddSilverProduct(quantity,false, me.doCartShare);
                }
            }
        },

        updateCartSilverProductsmarketingOptions: function(me){
            // var me = this;
            me.silverPlanProductAddedCount = 0;
            for (var i = 0; i < me.silverPlanProductIds.length; i++) {
                if(me.silverPlanProductIds[i].product.options.length === 0){
                    me.silverPlanProductIds[i].product.options = [];
                    me.silverPlanProductIds[i].product.options.push({
                        "attributeFQN":Hypr.getThemeSetting('attributesFQN').marketingOptin,
                        "shopperEnteredValue":true,
                        value:true
                    });
                }else{
                    for (var j = 0; j < me.silverPlanProductIds[i].product.options.length; i++) {
                        if(me.silverPlanProductIds[i].product.options[j].attributeFQN == Hypr.getThemeSetting('attributesFQN').marketingOptin){
                            me.silverPlanProductIds[i].product.options[j].value = true;
                        }
                    }
                }

                me.silverPlanProductAdd(me.silverPlanProductIds[i],me);

            }
            if(me.silverPlanProductIds.length === 0){
                me.doCartShare(me);
            }

        },
        silverPlanProductAddedCount : 0,
        silverPlanProductAdd : function(silverPlanProduct, me){
            api.request('put','/api/commerce/carts/current/items/'+silverPlanProduct.id, silverPlanProduct).then(function(BCProd){
                me.silverPlanProductAddedCount = me.silverPlanProductAddedCount +1;
                if(me.silverPlanProductAddedCount === me.silverPlanProductIds.length){
                    console.log('Silver Addded');
                    me.doCartShare(me);
                }
            });
        },

        doCartShare: function(me){
            // var me = this;me
            if(me.silverProductAdded && me.baseProductAdded){
                CartShare.cartShare().then(function(resp){
                    window.location.href="/cart";
                    sessionStorage.removeItem('cartItem');
                },function(err){
                    console.log(err);
                });
            }
        },

        apiCallToAddSilverProduct: function(quantity,marketingOptin,callback){
            var me = this;
            // Make sure current silver marketing option is selected
            var BCOptions = marketingOptin ? [
                {
                    "attributeFQN":Hypr.getThemeSetting('attributesFQN').marketingOptin,
                    "shopperEnteredValue":marketingOptin,
                    value:marketingOptin
                }
            ] : [];
            var BCPlanOb = {
                product: {
                    productCode: Hypr.getThemeSetting('bcInSilverCode'),
                    options: BCOptions
                },
                quantity: quantity,
                fulfillmentMethod: "Ship"
            };
            api.request('post', '/api/commerce/carts/current/items', BCPlanOb).then(function(res){
                me.silverProductAdded =  true;
                if(callback){
                    callback(me);
                }
            });
        },

        baseProductAdded : false,
        addNewBaseProduct: function(baseProduct, quantity){
            var me = this;
            // update option in base product to silver
            for (var i = 0; i < baseProduct.options.length; i++) {
                if(baseProduct.options[i].attributeFQN == Hypr.getThemeSetting('attributesFQN').bcProductFQN){
                    baseProduct.options[i].value = Hypr.getThemeSetting('bcInSilverCode');
                }
            }
            // set quantity
            var productOb = {
                product:{
                    productCode:baseProduct.productCode,
                    options: baseProduct.options
                },
                quantity:quantity,
                fulfillmentMethod: "Ship"
            };
            // add product
            api.request('post','/api/commerce/carts/current/items', productOb).then(function(mainProd){
                // Add silver plan product
                me.baseProductAdded = true;
                me.doCartShare();
            });
        }

	});

	var AddedToCartModel = Backbone.MozuModel.extend({});
	var SuppliesAccessoriesView = Backbone.MozuView.extend({
            templateName:'modules/product/added-to-cart-supp-acc',
            initialize:function(){
                var me=this;
                this.on('render', this.afterRender);
            },
            render:function(){
                var me = this;
                Backbone.MozuView.prototype.render.apply(this);
                $('.loader-wrapper').css('display','none');

            },
            afterRender: function() {
                if($(window).width() > 750 ){
                var addedSpupplies = $("ul.added-spupplies");
                        addedSpupplies.owlCarousel({
                            items: 4,
                            loop: false,
                            dots:false,
                            nav:true,
                            responsive:{
                                700:{
                                    items:3
                                },
                                1000:{
                                    items:4
                                },
                                1200:{
                                    items:4
                                }
                            }
                    });
                }
            },
            additionalEvents: {
               "click .addToCart": "addtocart"
            },
            addtocart : function(e){
                var obj = e.target;
                eventsModule.addToCart(obj);
            }
        });
	$(function(){
        CartMonitor.update();
		var cartitem;
		if(typeof(sessionStorage) != 'undefined'){
			cartitem = sessionStorage.getItem('cartItem');
            sessionStorage.removeItem('cartItem');
            if(cartitem === null){
                window.location.href = '/cart';
            }
            else{
    			var addedToCartModel = new AddedToCartModel(JSON.parse(cartitem));
    			var addedToCartView = new AddedToCartView({
    				el:$('#addedProductwrap'),
    				model:addedToCartModel
    			});
    			addedToCartView.render();
    			/*
    			* code for supplies and accessories
    			*/
    			var temp = $('#relatedProd').attr('data-mz-related-products');
    			temp = $.trim(temp);
    			var prodCode = temp.split(',');
    			var url = '/api/commerce/catalog/storefront/products/?filter=';
                for(var i=0;i<prodCode.length;i++){
                    if(i < prodCode.length-1)
                        url += 'productCode+eq+'+prodCode[i]+'+or+';
                    else
                        url += 'productCode+eq+'+prodCode[i];
                }
                url += '&pageSize=200';
                console.log(url);
                api.request('get',url,{}).then(function(response){
                	//getProductModel(response);
                    eventsModule.addToCart.objData = response;
                    var facetModel = new FacetModels.FacetedProductCollection(response);
                    console.log(facetModel);
                    var suppliesAccessoriesView = new SuppliesAccessoriesView({
                        el:$('#suppliesAccWrap'),
                        model: facetModel
                    });
                    suppliesAccessoriesView.render();
                });
            }
		}
		else{
			console.log('Session storage not supported.');
		}
        $('.loader-wrapper').hide();
	});

    /* Add to cart*/
    var eventsModule ={
        addToCart : function(obj){

            var productModule = productCustomActions.productModel($(obj).data("productid"),"script",eventsModule.addToCart.objData);
            //productModel.set('quantity',quantity);    SET QUANTITY.
            productModule.addToCart();
            productModule.on('addedtocart', function (cartitem) {
              if (cartitem && cartitem.prop('id')) {
                CartMonitor.addToCount(productModule.get('quantity'));
                /* added to cart event*/
                $(obj).addClass("added-to-cart").html(Hypr.getLabel("addedToCartSupp"));
              }
            });
        }
    };
    $(document).ready(function(){
        eventsModule.addToCart.objData = {};
    });
    /* Add to cart*/


});
