
var fs = require('fs');
var consoleLog = function(loger, fileWriteStream){
    if(fileWriteStream){
        fileWriteStream.write(loger);
        fileWriteStream.write("\n");
    }else {
        console.log(new Date() +"\t: "+loger);
    }
    process.stdout.write("|");
}


var getBackupWriter = function(){
    var fileWriteStream = fs.createWriteStream('./backup/mapping_'+new Date()+'.csv',{
        autoClose: true
    });
    consoleLog("PRODUCT_CODE,ENGLISH_CATEOGRY_ID,FRENCH_CATEGORY_ID",fileWriteStream);
    return fileWriteStream;
}


var start =  function(){
    getProductInfo(getBackupWriter(), 0);
}


var getProductInfo =  function(fileWriteStream, startIndex){
    var apiContext      = require('mozu-node-sdk/clients/platform/application')();
    apiContext.context.tenant = 21830;
    apiContext.context['master-catalog'] = 2;
    var productResource = require('mozu-node-sdk/clients/commerce/catalog/admin/product')(apiContext);

    productResource.getProducts({
        startIndex :  startIndex,
        pageSize  : 200
    }).then(function(response){
        // console.log(JSON.stringify(response));
        if(response.items.length>0){
            response.items.forEach(function(item,index){
                // console.log(JSON.stringify(item));
                // console.log("\n\n");
                consoleLog(
                    item.productCode+","+
                    (item.productInCatalogs && item.productInCatalogs[0] && item.productInCatalogs[0].productCategories ? item.productInCatalogs[0].productCategories.map(function(x){return x.categoryId; }).join("|") : "")+","+
                    (item.productInCatalogs && item.productInCatalogs[1] && item.productInCatalogs[1].productCategories ? item.productInCatalogs[1].productCategories.map(function(x){return x.categoryId; }).join("|") : ""),
                    fileWriteStream
                );
            });
            getProductInfo(fileWriteStream, startIndex+200);
        }
    }).catch(function(error){
        console.log(error);
    });
}


start();
