var PROD_TENANT_ID = 21830;
var XLSX = require('xlsx');
var COUNT = 0;
var apiContext      = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = PROD_TENANT_ID;
apiContext.context['master-catalog'] = 2;
var customerResource = require('mozu-node-sdk/clients/commerce/customer/customerAccount')(apiContext);
var fs = require('fs');


var start =  function(){
    readFromXLX();
}

var TOTAL = 0;
var readFromXLX =  function(){
    var workbook = XLSX.readFile('./getCustomerInfo/BP_WITHEMAIL_NOMOZUID.XLSX');
    // As we know our data is reside in firt sheet, lets open that
    var worksheet = workbook.Sheets[workbook.SheetNames[0]];
    // Convert data into a matrix strucutre ;), the best way to process
    // multi dimentional data! My Grandpa told me this.
    var xlsxData = XLSX.utils.sheet_to_json(worksheet, {header:1});
    // first row will be the properties as we know its a xlsx data :P
    console.log(xlsxData.length);
    TOTAL = xlsxData.length;
    var start = 0;
    // while(TOTAL > 0){
    // fetch10000(xlsxData,start,start+10000);
    fetch10000(xlsxData,0,10000);
    TOTAL = TOTAL - 10000;
    start = start +10000;
    // }
}

var writeToFile = function(KIBO_ACCOUNT_ID, KIBO_ADDRESS_ID, SAP_ADDRESS_ID, SAP_BP_NUMBER, index, fileWriteStream){
    COUNT =  COUNT + 1;
    process.stdout.write(COUNT+'|');
    fileWriteStream.write(KIBO_ACCOUNT_ID+","+SAP_BP_NUMBER+","+KIBO_ADDRESS_ID+","+SAP_ADDRESS_ID+","+new Date());
    fileWriteStream.write("\n");
}


var fetch10000 = function(xlsxData, start,end){
    var fileWriteStream = fileWriteStream = fs.createWriteStream('./getCustomerInfo/report/'+start+'-'+end+'-'+(new Date())+'.csv',{
        autoClose: true
    });
    fileWriteStream.write("KIBO_CUSTOMER_ID"+","+"SAP_BP_NUMBER"+","+"KIBO_ADDRESS_ID"+","+"SAP_ADDRESS_ID"+","+new Date());
    fileWriteStream.write("\n");
    for (var i = start; i < end; i++) {
        // console.log(xlsxData[i][0]);
        if(xlsxData[i] && xlsxData[i][0]){
            getData(xlsxData[i][0],fileWriteStream);
        }
    }
}
var ERROR_ITEMS = [];
var getData = function(id,fileWriteStream){
    customerResource.getAccounts({
        filter :  'externalId eq '+id,
        responseFields : 'items(externalId,contacts)'
    }).then(function(response){
        if(response.items.length == 0){
            process.stdout.write('⚽️'+id+', ')
        }else{
            process.stdout.write('😀'+id+', ')
            for (var i = 0; i < response.items.length; i++) {
                for (var j = 0; j < response.items[i].contacts.length; j++) {
                    writeToFile(
                        response.items[i].contacts[j].accountId, // KIBO ACCOUNT ID
                        response.items[i].contacts[j].id, // KIBO ADDRESS ID
                        response.items[i].contacts[j].address.address4, // SAP ADDRESS ID
                        response.items[i].externalId, // SAP BP NUMBER
                        index, fileWriteStream
                    );
                }
            }
        }
    }).catch(function(error){
        console.log("\nRE - ERROR "+id+"\n");
        if(ERROR_ITEMS.indexOf(id)>-1){
            getData(id, fileWriteStream);
            ERROR_ITEMS.push(id);
            console.log("\n\n");
            console.log(JSON.stringify(ERROR_ITEMS));
            console.log("\n\n");
        }
    });
}



// customerResource.getAccounts({
//     filter :  'externalId eq 8890001075',
//     responseFields : 'items(externalId,contacts)'
// }).then(function(response){
//     if(response.items.length == 0){
//         process.stdout.write('⚽️, ');
//     }else{
//         process.stdout.write('😀 ');
//     }
// }).catch(function(error){
//     console.log("\nERROR "+id+"\n");
//
//     console.log(error);
// });

start();
