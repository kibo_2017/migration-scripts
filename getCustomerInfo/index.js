var PROD_TENANT_ID = 21830;
var XLSX = require('xlsx');
var COUNT = 0;
var apiContext      = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = PROD_TENANT_ID;
apiContext.context['master-catalog'] = 2;
var customerResource = require('mozu-node-sdk/clients/commerce/customer/customerAccount')(apiContext);
var fs = require('fs');

var fileWriteStream = fileWriteStream = fs.createWriteStream('./getCustomerInfo/log/report__to_'+(new Date())+'.csv',{
    autoClose: true
});
fileWriteStream.write("KIBO_CUSTOMER_ID"+","+"SAP_BP_NUMBER"+","+"KIBO_ADDRESS_ID"+","+"SAP_ADDRESS_ID"+","+new Date());
fileWriteStream.write("\n");
var start =  function(){

    // getCustomers(['0000999019','0000999030','0000999036','0000999039','0001003310']);
    readFromXLX();
}

// 8890011299, 8890011303, 8890011305


var readFromXLX =  function(){
    var workbook = XLSX.readFile('./getCustomerInfo/BP_WITHEMAIL_NOMOZUID.XLSX');
    // As we know our data is reside in firt sheet, lets open that
    var worksheet = workbook.Sheets[workbook.SheetNames[0]];
    // Convert data into a matrix strucutre ;), the best way to process
    // multi dimentional data! My Grandpa told me this.
    var xlsxData = XLSX.utils.sheet_to_json(worksheet, {header:1});
    // first row will be the properties as we know its a xlsx data :P
    console.log(xlsxData.length);
    getCustomers(xlsxData,1,100);
}

var writeToFile = function(KIBO_ACCOUNT_ID, KIBO_ADDRESS_ID, SAP_ADDRESS_ID, SAP_BP_NUMBER, index){
    // if(COUNT == 5000 || COUNT == 0){
    //     COUNT = 1;
    //     fileWriteStream = fs.createWriteStream('./getCustomerInfo/log/report_'+index+'_to_'+(index+10000)+'.csv',{
    //         autoClose: true
    //     });
    //     fileWriteStream.write("KIBO_CUSTOMER_ID"+","+"SAP_BP_NUMBER"+","+"KIBO_ADDRESS_ID"+","+"SAP_ADDRESS_ID"+","+new Date());
    //     fileWriteStream.write("\n");
    //     process.stdout.write(index+'|');
    //     fileWriteStream.write(KIBO_ACCOUNT_ID+","+SAP_BP_NUMBER+","+KIBO_ADDRESS_ID+","+SAP_ADDRESS_ID+","+new Date());
    //     fileWriteStream.write("\n");
    // }else{
        COUNT =  COUNT + 1;
        process.stdout.write(COUNT+'|');
        fileWriteStream.write(KIBO_ACCOUNT_ID+","+SAP_BP_NUMBER+","+KIBO_ADDRESS_ID+","+SAP_ADDRESS_ID+","+new Date());
        fileWriteStream.write("\n");
    // }
}


var getCustomers1 =  function(info,start, end){
    var filters = [];
    for (var i = start; i < end; i++) {
        filters.push(info[i])
    }
    console.log(filters);
    var filterString =  "externalId eq '"+filters.join("' or externalId eq ");
    var filterString =  "externalId eq 1000131 or externalId eq 0000999019 or externalId eq 0000999030";
    // console.log(filterString);

}

var fetched = 0, PUSHED= [];
var getCustomers =  function(info,start, end){
    console.log(PUSHED);
    console.log(fetched);
    fetched =  0;

    for (var i = start; i < end; i++) {
        fetch(info,info[i],i,end);
    }
    // fetch(info,info[start],start,end);
}

var fetch =  function(info,id,index,end){
    // console.log(id[0]);
    // console.log(i+" > "+end);
    customerResource.getAccounts({
        filter :  'externalId eq '+id[0],
        responseFields : 'items(externalId,contacts)'
    }).then(function(response){
        if(response.items.length == 0){
            // writeToFile(
            //     "NOTFOUNT", // KIBO ACCOUNT ID
            //     "NOTFOUNT", // KIBO ADDRESS ID
            //     "NOTFOUNT", // SAP ADDRESS ID
            //     id, // SAP BP NUMBER
            //     index
            // );
            process.stdout.write('⚽️'+id+', ')
        }else{
            process.stdout.write('😀'+id+', ')
            for (var i = 0; i < response.items.length; i++) {
                for (var j = 0; j < response.items[i].contacts.length; j++) {
                    writeToFile(
                        response.items[i].contacts[j].accountId, // KIBO ACCOUNT ID
                        response.items[i].contacts[j].id, // KIBO ADDRESS ID
                        response.items[i].contacts[j].address.address4, // SAP ADDRESS ID
                        response.items[i].externalId, // SAP BP NUMBER
                        index
                    );
                }
            }
        }
        // if(index == end-1){
        //     getCustomers(info,end,end+150);
        // }
        fetched =  fetched+1;
        // console.log(fetched);
        // console.log(fetched+" == "+end+"-"+index);
        if(fetched == 99){
            getCustomers(info,end,end+100);
        }

    }).catch(function(error){
        fetched =  fetched+1;
        PUSHED.push(id);
        console.log("\nERROR "+id+"\n");
        // fetch(info,id,index,end);
        if(fetched == 99){
            getCustomers(info,end,end+100);
        }
    });

}
/**
    8890019246
    8890026222
    8890030811
    8890032583
    8890031855
    8890032784
    8890033572
    8890032145
    8890032087
    8890032173
    8890032213
    8890033118
    8890032339
    8890032373
    8890032897
    8890033177
    8890034338
    8890034338

**/

start();
