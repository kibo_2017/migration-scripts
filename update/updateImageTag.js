
var TENANT_ID = 17149;
var ENTITY_GROUP_NAME = 'content_item_info_list_1@Brother';


var apiContext = require('mozu-node-sdk/clients/platform/application')();
apiContext.context.tenant = TENANT_ID;
var entityResource     = require('mozu-node-sdk/clients/platform/entitylists/entity')(apiContext);


var cheerio = require('cheerio');


var consoleLog = function(str){
    process.stdout.write(str+'');
}

var updateImgSrcTag =  function(item, startIndex){
    if(item.data.config.model['html-editor']){
        var $ = cheerio.load(item.data.config.model['html-editor']);
        // console.log('\t\t   : '+$('img').length);
        if($('img').length > 0){
            $('img').each(function(index, value){
                var src = $(this).attr('src');
                $(this).removeAttr('src');
                $(this).attr('data-src',src);
            });
            item.data.config.model['html-editor'] = $('body').html();
            updateEntity(item);
        }else {
            consoleLog("  NA  ");
        }
    }
}


var updateEntity =  function(item){
    entityResource.updateEntity(item).then(function(response){
        consoleLog("***");
    }).catch(function(error){
        consoleLog(error);
    });
}

var getEntities = function(startIndex){
    var requestBody = {
        'entityListFullName': ENTITY_GROUP_NAME,
        'startIndex'        : 600,
        'pageSize'          : 200
    };
    entityResource.getEntities(requestBody).then(function(response){
        console.log(response.items.length);
        for (var i = 0; i < response.items.length; i++) {
            updateImgSrcTag(response.items[i],startIndex);
        }
    }).catch(function(error){
        consoleLog(error);
    });
}

getEntities(0);
